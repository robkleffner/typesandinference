# Overview

These projects are basic, minimal but complete implementations of HM-style type inference with some neat extensions that maintain principle type schemes. Though the type systems are based on and inspired by Hindley-Milner, the algorithm used is originally by Adam Gundry (big thanks to his work on type inference in the presence of 'Abelian group' types). The big reason for choosing Gundry-style inference was to 1) become fully intimate with the algorithm, 2) be capable of handling Abelian group types, and 3) eventually combine as many of these separate systems as possible, which is much easier if they all use the same algorithmic style. One of the type systems, **Inference.Dots**, is my own original research on type inference for 'variable-arity polymorphism'.

I started this project in C#, but translated it to F# because I haven't found a parsimonious way to define ASTs in OO languages. If you think this F# iteration is boilerplatey, you should have seen the C# version.

A more complete description of each project is found below.

## Inference.Common

A few utilities that get used across the projects that were easy to abstract.

## Inference.Basic

The standard Hindley-Milner type system, with type inference implemented in a simplified version of 'type inference in context', a.k.a. Gundry-style inference.

## Inference.Typeclasses

Standard Hindley-Milner extended with single-parameter qualified types, similar to those in Haskell. I did not implement superclasses, but it's an easy addition if you need it. Mostly an adaptation of Mark Jones' 'Typing Haskell in Haskell' for Gundry-style inference.

## Inference.Units

Standard Hindley-Milner extended with units of measure. Based on Adam Gundry's papers on the subject.

## Inference.ScopedLabels

Standard Hindley-Milner extended with extensible records and variants. I adapted Daan Leijen's research on the subject for Gundry-style inference, which ended up being a bit more verbose than I expected. The unification of rows, in particular, is bigger and more complex than that found in Leijen's research, but should be equivalent.

## Inference.Uniqueness

Standard Hindley-Milner extended with uniqueness and linearity attributes on ALL types. Adapted from the research paper by Edsko de Vries. The great insight in Edsko's research is that uniqueness and linearity can be enforced using the same type system, with careful library design. This one definitely is the most at risk when trying to support other integrations: it's 'viral' in that it affects every single type that gets generated, although it doesn't complicate the type AST or unification much.

## Inference.Dots

Standard Hindley-Milner extended with variable-arity polymorphism, and using original research I have done on unification for dotted types. The type system is based on a system first explored by Strickland, Tobin-Hochstadt, and Felleisen. I made some changes to the type system to support dotted types that contain more than one 'pretype' variable, and partial substitution/extension of such types. Strickland et al. also don't explore type inference much in their paper. My project should show that HM with dotted types is sound and complete, and has principle type schemes. It is fairly easy to tell from the code that 'dotted unification' terminates, but I would still need to run the proofs to be fully sure of the system.

The neat thing about dotted types is that we get types for variable arity tuples:

```
tupleHead : (a,b...) -> a
tupleTail : (a,b...) -> (b...)
tupleCons : a -> (b...) -> (a,b...)
```

Even though we don't have arbitrary recursion over the size of tuples, we do get some suprising operations that we can perform on variable-arity tuples:

```
tupleMap : (a -> b) -> (a...) -> (b...)
tupleAny : (a -> Bool) -> (a...) -> Bool
tupleAll : (a -> Bool) -> (a...) -> Bool

// zip an arbitrary number of lists! no more zip2, zip3, zip4, etc.
zip : ([a]...) -> [(a...)]

// we can even do a type of fold over tuples
tupleFoldl : (c -> (a...) -> c) -> c -> ([a]...) -> c

// for async languages, we could get a parametrically polymorphic version of Task.WhenAll
whenAll : (Task<a>...) --async-> (a...)
```

For more cool examples of dotted types, see the Practical Variable-Arity Polymorphism paper.
