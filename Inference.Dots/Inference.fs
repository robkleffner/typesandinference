﻿namespace Inference.Dots

module Inference =

    open Inference.Common.Common;
    open Kinds;
    open DotSeq;
    open Types;
    open Terms;
    open Environment;

    type InferenceState = { Fresh: int seq; Environment: List<EnvironmentEntry> }


    // utilities for working with inference states
    let extractConstraint context =
        List.tryFindIndex (isConstraint) context
        |> Option.map (fun constrInd -> (List.take constrInd context, List.item constrInd context, List.skip (constrInd + 1) context))

    let freshVar (prefix: string) =
        state
            {
            let! inf = get
            let i = Seq.head inf.Fresh
            do! put { Fresh = Seq.tail inf.Fresh; Environment = inf.Environment }
            return prefix.Substring(0, 1) + i.ToString()
            }

    let getEnvironment =
        state
            {
            let! inf = get
            return inf.Environment
            }

    let updateEnvironment f =
        state
            {
            let! inf = get
            do! put { Fresh = inf.Fresh; Environment = f inf.Environment }
            }

    let putEnvironment context = updateEnvironment (constant context)

    let pushEntry entry = updateEnvironment (fun con -> List.append con [entry])

    let addIntro name kind = pushEntry (typeVarIntro name kind)

    let addBinding name ty = pushEntry (termVarBind name ty)

    let popBinding name =
        let nameBinding item =
            match item with
            | ETermVarBind (n, _) when n = name -> true
            | _ -> false

        updateEnvironment (List.filter (nameBinding >> not))


    // constraint solving in context
    let solveFlexFlex left right kind linds rinds top =
        match top with
        | ETypeVarIntro (n, k) ->
            match (left = n, right = n) with
            | (true, true) -> [top]
            | (true, false) -> [typeVarDef n (typeVar right kind rinds)]
            | (false, true) -> [typeVarDef n (typeVar left kind linds)]
            | (false, false) -> [flexFlexConstr left right kind linds rinds; top]
        | ETypeVarDef (n, _) when left = n && right = n ->
            [top]
        | ETypeVarDef (n, def) ->
            [simpleBinaryConstr (typeSubst n def (typeVar left kind linds)) (typeSubst n def (typeVar right kind rinds)); top]
        | _ ->
            [flexFlexConstr left right kind linds rinds; top]

    let solveFlexRigid name kind inds ty deps top =
        match top with
        | ETypeVarDef (n, def) ->
            let constr =
                if (typeFree ty |> Set.add name).Contains n
                then simpleBinaryConstr (typeSubst n def (typeVar name kind inds)) (typeSubst n def ty)
                else flexRigidConstr name kind inds ty []
            List.append deps [constr; top]
        | ETypeVarIntro (n, k) ->
            match (name = n, (typeFree ty).Contains(n)) with
            | (true, true) -> failwith "Occurs check failed"
            | (true, false) -> List.append deps [typeVarDef name ty]
            | (false, true) -> [flexRigidConstr name kind inds ty (top :: deps)]
            | (false, false) -> [flexRigidConstr name kind inds ty deps; top]
        | _ -> [flexRigidConstr name kind inds ty deps; top]

    let solveTypeConstraint left right =
        state
            {
            match (left, right) with
            | (TCon (ln, lk), TCon (rn, rk)) when ln = rn && lk = rk ->
                return []
            | (TApp (ll, lr), TApp (rl, rr)) ->
                return [simpleBinaryConstr ll rl; simpleBinaryConstr lr rr]
            | (TVar (ln, lk, li), TVar (rn, rk, ri)) when lk = rk ->
                return [flexFlexConstr ln rn lk li ri]
            | (TSeq l, TSeq r) ->
                return [seqConstr l r]
            | (TVar (ln, lk, li), r) when lk = typeKind r ->
                return [flexRigidConstr ln lk li r []]
            | (l, TVar (rn, rk, ri)) when rk = typeKind l ->
                return [flexRigidConstr rn rk ri l []]
            | _ ->
                return failwith "Rigid-rigid mismatch or kinds do not unify"
            }

    let rec genSplitSub vars =
        state
            {
            match vars with
            | [] -> return Map.empty, Map.empty, Map.empty, []
            | x :: xs ->
                let! freshInd = freshVar x
                let! freshSeq = freshVar x
                let! (inds, seqs, sub, fresh) = genSplitSub xs
                return
                    Map.add x (typeVar freshInd KData []) inds,
                    Map.add x (typeVar freshSeq KData []) seqs,
                    Map.add x (TSeq (SInd (typeVar freshInd KData [], (SDot (typeVar freshSeq KData [], SEnd))))) sub,
                    freshInd :: freshSeq :: fresh
            }

    let solveSeqConstraint left right =
        state
            {
            match left, right with
            | SInd (li, lr), SInd (ri, rr) -> return [simpleBinaryConstr li ri; seqConstr lr rr]
            | SEnd, SEnd -> return []
            | SDot (li, SEnd), SDot (ri, SEnd) -> return [simpleBinaryConstr li ri]
            | SDot (li, SEnd), SEnd ->
                let dottedMapped = [for v in List.ofSeq (typeFree li) do (v, TSeq SEnd)] |> Map.ofList
                return [substConstr dottedMapped []]
            | SEnd, SDot (ri, SEnd) ->
                let dottedMapped = [for v in List.ofSeq (typeFree ri) do (v, TSeq SEnd)] |> Map.ofList
                return [substConstr dottedMapped []]
            | SDot (li, SEnd), SInd (ri, rs) ->
                let free = typeFree li |> List.ofSeq
                let! inds, seqs, sub, fresh = genSplitSub free
                return List.append
                    [for v in fresh do yield typeVarIntro v KData]
                    [substConstr sub []; simpleBinaryConstr (applySubstType inds li) ri; seqConstr (SDot (applySubstType seqs li, SEnd)) rs]
            | SInd (li, ls), SDot (ri, SEnd) ->
                let free = typeFree li |> List.ofSeq
                let! inds, seqs, sub, fresh = genSplitSub free
                return List.append
                    [for v in fresh do yield typeVarIntro v KData]
                    [substConstr sub []; simpleBinaryConstr (applySubstType inds ri) li; seqConstr (SDot (applySubstType seqs ri, SEnd)) ls]
            | _ -> return failwith "Sequences do not unify"
            }

    let solveSubstConstraint subst deps top =
        if Map.isEmpty subst
        then List.append deps [top]
        else
            match top with
            | ETypeVarDef (n, def) ->
                let updatedSubst = Map.remove n (Map.map (fun _ v -> typeSubst n def v) subst)
                if Map.exists (fun _ v -> (typeFree v).Contains n) subst
                then [ESubstConstraint (updatedSubst, top :: deps)]
                else List.append deps [ESubstConstraint (updatedSubst, []); top]
            | ETypeVarIntro (n, k) ->
                match (subst.ContainsKey n, Map.exists (fun _ v -> (typeFree v).Contains n) subst) with
                | (true, true) -> List.append deps [typeVarDef n subst.[n]; ESubstConstraint (Map.remove n subst, [])]
                | (true, false) -> List.append deps [ESubstConstraint (Map.remove n subst, []); typeVarDef n subst.[n]]
                | (false, true) -> [ESubstConstraint (subst, top :: deps)]
                | (false, false) -> [ESubstConstraint (subst, deps); top]
            | _ -> [ESubstConstraint (subst, deps); top]
            
    let solveInPlace prefix suffix fn =
        state
            {
            let! constrs = fn
            do! append3 prefix constrs suffix |> putEnvironment
            }

    let solveOrMoveUp prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        append3 popped (fn top) suffix |> putEnvironment

    let solveOrMoveUpWithState prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        state
            {
            let! constrs = fn top
            do! append3 popped constrs suffix |> putEnvironment
            }

    let solveConstraint prefix suffix constr =
        match constr with
        | ETypeConstraint (left, right) ->
            solveInPlace prefix suffix (solveTypeConstraint left right)
        | EFlexFlexConstraint (left, right, kind, lind, rind) ->
            solveOrMoveUp prefix suffix (solveFlexFlex left right kind lind rind)
        | EFlexRigidConstraint (var, kind, inds, rigid, deps) ->
            solveOrMoveUp prefix suffix (solveFlexRigid var kind inds rigid deps)
        | ESeqConstraint (left, right) ->
            solveInPlace prefix suffix (solveSeqConstraint left right)
        | ESubstConstraint (subst, deps) ->
            solveOrMoveUp prefix suffix (solveSubstConstraint subst deps)
        | _ -> failwith "Tried to solve a non-constraint context entry"

    let rec solve () =
        state
            {
            let! env = getEnvironment
            match extractConstraint env with
            | Some (prefix, constr, suffix) ->
                do! solveConstraint prefix suffix constr
                do! solve ()
            | None -> do! putEnvironment env
            }


    // inference: from terms to typed terms
    let specialize poly =
        let rec specialLoop quant body =
            state
                {
                match quant with
                | q :: qs ->
                    let! fresh = freshVar (fst q)
                    do! addIntro fresh (snd q)
                    return! specialLoop qs (typeSubst (fst q) (typeVar fresh (snd q) []) body)
                | [] -> return body
                }
        specialLoop poly.Quantified poly.Body

    let genTupleType size =
        let rec genTupleRec tup size =
            state
                {
                match size with
                | 0 -> return typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData))) (typeSeq tup)
                | n ->
                    let! fresh = freshVar "t"
                    do! addIntro fresh KData
                    let! tup = genTupleRec (SInd (typeVar fresh KData [], tup)) (n - 1)
                    return funType (typeVar fresh KData []) tup
                }
        genTupleRec SEnd size
            

    let unify left right =
        state
            {
            do! pushEntry (simpleBinaryConstr left right)
            do! solve ()
            }

    let skimContext =
        state
            {
            let! context = getEnvironment
            let skimmed = List.rev context |> List.takeWhile (isLocalityMark >> not)
            do! putEnvironment (List.rev context |> List.skipWhile (isLocalityMark >> not) |> List.skip 1 |> List.rev)
            return skimmed
            }

    let rec infer term =
        state
            {
            match term with
            | BVar v ->
                let! context = getEnvironment
                match getTermVarType v context with
                | Some t -> return! specialize t
                | None -> return failwith "Could not find binding in context"
            | BAbs (p, b) ->
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! addBinding p (schemeType [] (typeVar fresh KData []))
                let! rinf = infer b
                do! popBinding p
                return funType (typeVar fresh KData []) rinf
            | BApp (l, r) ->
                let! linf = infer l
                let! rinf = infer r
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! unify linf (funType rinf (typeVar fresh KData []))
                return typeVar fresh KData []
            | BLet (l, e, b) ->
                let! geninf = inferGeneralized e
                do! addBinding l geninf
                let! bodyinf = infer b
                do! popBinding l
                return bodyinf
            | BTuple len ->
                return! genTupleType len
            }
    and inferGeneralized term =
        state
            {
            do! pushEntry ELocalityMark
            let! ungen = infer term
            let! generalized = skimContext
            return makeScheme generalized ungen
            }

    let inferFrom environment term =
        let (inf, final) = run { Fresh = Seq.initInfinite id; Environment = environment } (infer term)
        let normalized = normalizeEnv final.Environment
        applyEnvType normalized inf

    let inferFromEmpty term = inferFrom [] term