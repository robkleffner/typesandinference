﻿namespace Inference.Dots

module Environment =

    open Kinds
    open Types
    
    type EnvironmentEntry =
        | ETypeVarIntro of string * Kind
        | ETypeVarDef of string * Type
        | ETermVarBind of string * TypeScheme
        | ELocalityMark
        | ETypeConstraint of left: Type * right: Type
        | EFlexFlexConstraint of left: string * right: string * kind: Kind * leftInds: List<int> * rightInds: List<int>
        | EFlexRigidConstraint of var: string * kind: Kind * inds: List<int> * rigid: Type * dependencies: List<EnvironmentEntry>
        | ESeqConstraint of left: DotSeq.DotSeq<Type> * right: DotSeq.DotSeq<Type>
        | ESubstConstraint of subst: Map<string, Type> * dependencies: List<EnvironmentEntry>
    
    
    // utilities for working with environment entries
    let typeVarIntro name kind = ETypeVarIntro (name, kind)
    
    let typeVarDef name def = ETypeVarDef (name, def)
    
    let termVarBind name ty = ETermVarBind (name, ty)
    
    let simpleBinaryConstr left right = ETypeConstraint (left, right)
    
    let flexFlexConstr left right kind lInds rInds = EFlexFlexConstraint (left, right, kind, lInds, rInds)
    
    let flexRigidConstr var kind inds rigid deps = EFlexRigidConstraint (var, kind, inds, rigid, deps)

    let seqConstr left right = ESeqConstraint (left, right)

    let substConstr subst deps = ESubstConstraint (subst, deps)
    
    let isConstraint entry =
        match entry with
        | ETypeConstraint _ -> true
        | EFlexFlexConstraint _ -> true
        | EFlexRigidConstraint _ -> true
        | ESeqConstraint _ -> true
        | ESubstConstraint _ -> true
        | _ -> false
    
    let isLocalityMark entry =
        match entry with
        | ELocalityMark -> true
        | _ -> false
    
    let typeVarName entry =
        match entry with
        | ETypeVarIntro (n, _) -> n
        | ETypeVarDef (n, _) -> n
        | _ -> failwith "Expected a type variable entry"
    
    let constraintFields fields = List.map fst fields |> Set.ofList
    
    let sharedConstrainFields lefts rights = Set.intersect (constraintFields lefts) (constraintFields rights)
    
    
    // substitution computations
    let applyEntryType t entry =
        match entry with
        | ETypeVarDef (n, def) -> typeSubst n def t
        | _ -> t
    
    let applyEnvType env t = List.fold applyEntryType t env
    
    
    // accessors over contexts
    let rec getTermVarType var env =
        match env with
        | ETermVarBind (n, t) :: _ when n = var -> Option.Some t
        | _ :: cs -> getTermVarType var cs
        | [] -> Option.None
    
        
    // other utilities
    let makeScheme generalized ty =
        let mutable body = ty
        let mutable quantified = List.empty
        for g in generalized do
            match g with
            | ETypeVarIntro (n, k) -> quantified <- (n, k) :: quantified
            | ETypeVarDef (n, d) -> body <- typeSubst n d body
            | _ -> failwith "Unexpected entry in skimmed context"
        schemeType quantified body
    
    let normalizeEntry env entry =
        match entry with
        | ETypeVarDef (n, t) -> ETypeVarDef (n, applyEnvType env t)
        | _ -> entry
    
    let normalizeEnv = List.fold (fun prev next -> normalizeEntry prev next :: prev) [] >> List.rev