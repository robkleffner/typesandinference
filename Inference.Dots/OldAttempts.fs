﻿namespace Inference.Dots

module DotsAsConstructor =
    open Kinds

    type Type =
        | TVar of name: string * kind: Kind * indexes: int list
        | TCon of name: string * kind: Kind
        | TApp of left: Type * right: Type
        | TSeq of types: Type list
        | TDot of dotted: Type

    let rec free (t : Type) =
        match t with
        | TVar(n, _, _) -> Set.singleton(n)
        | TCon(_, _) -> Set.empty<string>
        | TApp(l, r) -> free l + free r
        | TSeq(ts) ->
            [ for t in ts do yield free t ]
            |> Set.unionMany
        | TDot(t) -> free t

    let rec isHeadNormalForm(t : Type) =
        match t with
        | TVar(_, _, _) -> true
        | TApp (left, _) -> isHeadNormalForm left
        | _ -> false

    let rec kind(t : Type) =
        match t with
        | TVar(_, k, _) -> k
        | TCon(_, k) -> k
        | TApp(l, r) ->
            match kind l with
            | KArrow(al, ar) -> if (al = kind r) then ar else failwith "Kind error: tried to apply type constructor to arg of wrong kind"
            | _ -> failwith "Kind error: type constructor requires arrow kind"
        | TSeq _ -> KSeq KData
        | TDot t -> kind t

    let isSeq ty =
        match ty with
        | TSeq _ -> true
        | TDot (TSeq _) -> true
        | _ -> false

    let isNotSeq ty =
        match ty with
        | TSeq _ -> false
        | TDot (TSeq _) -> false
        | _ -> true

    let dvar name = TVar (name, KData, [])

    let divar name inds = TVar (name, KData, inds)

module ExtendAndSubstitute =
    open DotsAsConstructor

    let rec getsub (inds : int list) (sub : Type) =
        match (inds, sub) with
        | ([], s) -> s
        | ([i], TSeq ts) ->
            match ts.[i] with
            | TDot t -> t
            | any -> any
        | (i :: is, TSeq ts) ->
            match ts.[i] with
            | TDot t -> getsub is t
            | any -> getsub is any
        | _ -> failwith "Could not get substitution, index matched with non-sequence substitution value."

    let zipwith (l1 : 'a list) (l2 : 'b list) (f : 'a -> 'b -> 'c) =
        List.zip l1 l2 |> List.map (fun (l,r) -> f l r)

    let zipn items = 
        Seq.collect Seq.indexed items
        |> Seq.groupBy fst 
        |> Seq.map (snd >> Seq.map snd)
        |> Seq.map Seq.toList
        |> Seq.toList
    
    let rec dotLevel (t : Type) =
        match t with
        | TDot tp -> 1 + dotLevel tp
        | TSeq ts -> List.map dotLevel ts |> List.max
        | TApp (l, r) -> max (dotLevel l) (dotLevel r)
        | _ -> 0

    let rec dotLevelForVar (v : string) (t : Type) =
        match t with
        | TVar (tv, _, inds) -> 0//if v = tv then inds.Length else 0
        | TDot tp -> if (free tp).Contains(v) then 1 + dotLevelForVar v tp else dotLevelForVar v tp
        | TSeq ts -> List.map (dotLevelForVar v) ts |> List.max
        | TApp (l, r) -> max (dotLevelForVar v l) (dotLevelForVar v r)
        | _ -> 0

    let rec substSeqLevel (t : Type) =
        match t with
        | TSeq ts -> 1 + (List.map substSeqLevel ts |> List.max)
        | TDot t -> substSeqLevel t
        | _ -> 0

    let rec seqLevelForVar (v : string) (t : Type) =
        match t with
        //| TVar (tv, _, inds) -> if v = tv then inds.Length else 0
        | TSeq ts -> 1 + (List.map (seqLevelForVar v) ts |> List.max)
        | TDot td -> seqLevelForVar v td
        | TApp (l, r) -> max (seqLevelForVar v l) (seqLevelForVar v r)
        | _ -> 0

    let seqLen (t: Type) =
        match t with
        | TSeq ts -> ts.Length
        | TDot (TSeq ts) -> ts.Length
        | _ -> 0

    let seqSubs (t: Type) =
        match t with
        | TSeq ts -> ts
        | TDot (TSeq ts) -> ts
        | _ -> []

    let rec extend (t: Type) (len: int) =
        match t with
        | TVar(n, k, inds) -> if len = 1 then [ t ] else [ for i in 0..len-1 do yield TVar(n, k, List.append inds [i]) ]
        | TCon(_, _) -> [ for i in 0..len-1 do yield t ]
        | TApp(l, r) -> zipwith (extend l len) (extend r len) (fun l r -> TApp(l, r))
        | TSeq(ts) -> [ for t in ts do yield extend t len ] |> zipn |> List.map TSeq
        | TDot(t) -> [ for t' in extend t len do yield TDot t' ]

    let spliceDot t =
        match t with
        | TDot (TSeq ts) -> ts
        | t -> [t]

    let spliceDots ts = List.map spliceDot ts |> List.concat

    let applyDot s t =
        match s with
        | TDot _ -> TDot t
        | _ -> t

    let rec extendForVar (v : string) (ty : Type) (sub : Type) =
        let rec extendLevel ty sub lvl =
            let subLvl = substSeqLevel sub
            if lvl >= subLvl
            then
                match ty with
                //| TVar (tv, _, inds) -> if (tv = v && subLvl > 0) then extendLevel ty (getsub inds sub) lvl else ty
                | TApp (l, r) -> TApp (extendLevel l sub lvl, extendLevel r sub lvl)
                | TSeq ts -> List.map (fun t -> extendLevel t sub lvl) ts |> spliceDots |> TSeq
                | TDot t ->
                    let newLevel = if (free t).Contains v then lvl - 1 else lvl
                    extendLevel t sub newLevel |> TDot
                | _ -> ty
            else
                extend ty (seqLen sub)
                |> List.zip (seqSubs sub)
                |> List.map (fun (s, t) -> extendLevel t s lvl |> applyDot s)
                |> TSeq
        
        if (free ty).Contains v
        then extendLevel ty sub (seqLevelForVar v ty)//(dotLevelForVar v ty)
        else ty

    let rec consIndex (v : string) (i : int) (ty : Type) =
        match ty with
        | TApp (l, r) -> TApp (consIndex v i l, consIndex v i r)
        | TSeq ts -> List.map (fun t -> consIndex v i t) ts |> TSeq
        | TDot t -> TDot (consIndex v i t)
        | TVar (tv, k, inds) -> TVar (tv, k, i :: inds)
        | any -> any

    let rec newCopyN (t: Type) (len: int) =
        match t with
        | TVar(n, k, inds) -> [ for i in 0..len-1 do yield TVar(n, k, inds) ]
        | TCon(_, _) -> [ for i in 0..len-1 do yield t ]
        | TApp(l, r) -> zipwith (newCopyN l len) (newCopyN r len) (fun l r -> TApp(l, r))
        | TSeq(ts) -> [ for t in ts do yield newCopyN t len ] |> zipn |> List.map TSeq
        | TDot(t) -> [ for t' in newCopyN t len do yield TDot t' ]

    let rec seqIndLevelForVar (v : string) (t : Type) =
        match t with
        | TVar (tv, _, inds) -> if v = tv then -inds.Length else System.Int32.MaxValue
        | TSeq ts -> 1 + (List.map (seqIndLevelForVar v) ts |> List.min)
        | TDot td -> seqIndLevelForVar v td
        | TApp (l, r) -> min (seqIndLevelForVar v l) (seqIndLevelForVar v r)
        | _ -> System.Int32.MaxValue

    let rec extendNew (v : string) (ty : Type) (sub : Type) =
        let freeVars = free ty
        let dotLvl = dotLevelForVar v ty
        let subLvl = substSeqLevel sub
        let indLvl = seqIndLevelForVar v ty
        match ty with
        | any when subLvl <= 0 -> any
        | any when not (freeVars.Contains v) -> any
        | any when dotLvl >= subLvl ->
            match any with
            | TApp (l, r) -> TApp (extendNew v l sub, extendNew v r sub)
            | TSeq ts ->
                if indLvl = 0
                then List.zip (seqSubs sub) ts |> List.mapi (fun i (s, t) -> extendNew v t s) |> TSeq
                else List.map (fun t -> extendNew v t sub) ts |> spliceDots |> TSeq
            | TDot t -> extendNew v t sub |> TDot
            | _ -> ty
        | any ->
            newCopyN any (seqLen sub)
            |> List.zip (seqSubs sub)
            |> List.mapi (fun i (s, t) -> extendNew v t s |> applyDot s |> consIndex v i)
            |> TSeq

    let rec substitute (rep : Type) (target: string) (sub: Type) =
        match rep with
        | TCon(_, _) -> rep
        | TVar(n, _, inds) -> if n = target then getsub inds sub else rep
        | TSeq(ts) ->
            List.map (fun t -> substitute t target sub) ts
            |> TSeq
        | TApp(l,r) -> TApp (substitute l target sub, substitute r target sub)
        | TDot d -> TDot (substitute d target sub)

    let extendAndSubstitute (rep : Type) (target : string) (sub : Type) =
        substitute (extendForVar target rep sub) target sub

module Simultaneous =

    open DotsAsConstructor

    let rec getsub (inds : int list) (sub : Type) =
        match (inds, sub) with
        | ([], s) -> s
        | ([i], TSeq ts) -> ts.[i]
        | (i :: is, TSeq ts) -> getsub is ts.[i]
        | _ -> failwith "Could not get substitution, index matched with non-sequence substitution value."

    let zipwith (l1 : 'a list) (l2 : 'b list) (f : 'a -> 'b -> 'c) =
        List.zip l1 l2 |> List.map (fun (l,r) -> f l r)

    let zipn items = 
        Seq.collect Seq.indexed items
        |> Seq.groupBy fst 
        |> Seq.map (snd >> Seq.map snd)
        |> Seq.map Seq.toList
        |> Seq.toList

    let spliceDot t =
        match t with
        | TDot (TSeq ts) -> ts
        | t -> [t]

    let spliceDots ts = List.map spliceDot ts |> List.concat

    let rec extend (t: Type) (len: int) =
        match t with
        | TVar(n, k, inds) -> [ for i in 0..len-1 do yield TVar(n, k, inds) ]
        | TCon(_, _) -> [ for i in 0..len-1 do yield t ]
        | TApp(l, r) -> zipwith (extend l len) (extend r len) (fun l r -> TApp(l, r))
        | TSeq(ts) -> [ for t in ts do yield extend t len ] |> zipn |> List.map TSeq
        | TDot(t) -> [ for t' in extend t len do yield TDot t' ]

    let extractDot (t : Type) =
        match t with
        | TApp (TDot l, TDot r) -> TDot (TApp (l, r))
        | TApp (TDot l, r) -> TDot (TApp (l, r))
        | TApp (l, TDot r) -> TDot (TApp (l, r))
        | TApp (l, r) -> TApp (l, r)
        | _ -> failwith "Called extractDot on non TApp"

    let rec fixApp (t : Type) =
        match t with
        | TDot td -> TDot (fixApp td)
        | TApp (TSeq ls, TSeq rs) -> zipwith ls rs (fun l r -> TApp (l, r) |> extractDot |> fixApp) |> TSeq
        | TApp (TSeq ls, r) -> zipwith ls (extend r ls.Length) (fun l r -> TApp (l, r) |> extractDot |> fixApp) |> TSeq
        | TApp (l, TSeq rs) -> zipwith (extend l rs.Length) rs (fun l r -> TApp (l, r) |> extractDot |> fixApp) |> TSeq
        | TApp (l, r) -> TApp (l, r) |> extractDot
        | _ -> failwith "Called fixApp on non TApp"

    let rec fixSeq (t : Type) =
        match t with
        | TSeq ts -> spliceDots ts |> TSeq
        | _ -> failwith "Called fixSeq on non TSeq"

    let rec substitute (rep : Type) (target: string) (sub: Type) =
        match rep with
        | TCon(_, _) -> rep
        | TVar(n, _, inds) -> if n = target then getsub inds sub else rep
        | TSeq(ts) -> List.map (fun t -> substitute t target sub) ts |> TSeq |> fixSeq
        | TApp(l,r) -> TApp (substitute l target sub, substitute r target sub) |> fixApp
        | TDot d -> TDot (substitute d target sub)