﻿namespace Inference.Dots

module DotSeq =

    type DotSeq<'a> =
        | SInd of ty: 'a * rest: DotSeq<'a>
        | SDot of ty: 'a * rest: DotSeq<'a>
        | SEnd

        override this.ToString () =
            match this with
            | SInd (t, SEnd) -> $"{t}"
            | SInd (t, ts) -> $"{t},{ts}"
            | SDot (t, SEnd) -> $"{t}..."
            | SDot (t, ts) -> $"{t}...,{ts}"
            | SEnd -> ""

    let rec ofList (ts : 'a list) =
        match ts with
        | [] -> SEnd
        | t :: ts -> SInd (t, ofList ts)

    let rec toList (ts : DotSeq<'a>) =
        match ts with
        | SInd (i, rs) -> i :: toList rs
        | SDot (i, rs) -> i :: toList rs
        | SEnd -> []

    let rec length (ts : 'a DotSeq) =
        match ts with
        | SInd (_, rs) -> 1 + length rs
        | SDot (_, rs) -> 1 + length rs
        | SEnd -> 0

    let rec map (f : 'a -> 'b) (ts : 'a DotSeq) =
        match ts with
        | SInd (b, rs) -> SInd (f b, map f rs)
        | SDot (b, rs) -> SDot (f b, map f rs)
        | SEnd -> SEnd

    let rec all (f : 'a -> bool) (ts : 'a DotSeq) =
        match ts with
        | SInd (b, rs) -> f b && all f rs
        | SDot (b, rs) -> f b && all f rs
        | SEnd -> true

    let rec any (f : 'a -> bool) (ts : 'a DotSeq) =
        match ts with
        | SInd (b, rs) -> f b || any f rs
        | SDot (b, rs) -> f b || any f rs
        | SEnd -> false

    let rec anyIndInSeq (ts : 'a DotSeq) =
        match ts with
        | SInd _ -> true
        | SEnd -> false
        | SDot (_, rs) -> anyIndInSeq rs

    let rec append (ls : 'a DotSeq) (rs : 'a DotSeq) =
        match ls with
        | SEnd -> rs
        | SInd (t, sls) -> SInd (t, append sls rs)
        | SDot (t, sls) -> SDot (t, append sls rs)

    let rec at (ind : int) (tyseq : 'a DotSeq) =
        match tyseq with
        | SInd (t, rs) -> if ind = 0 then t else at (ind - 1) rs
        | SDot (t, rs) -> if ind = 0 then t else at (ind - 1) rs
        | SEnd -> failwith "Tried to index an element outside the bounds of the sequence."

    let rec zipwith (ls : 'a DotSeq) (rs : 'b DotSeq) (f : 'a -> 'b -> 'c) =
        match (ls, rs) with
        | (SInd (lb, ls), SInd (rb, rs)) -> SInd ((f lb rb), zipwith ls rs f)
        | (SDot (lb, ls), SDot (rb, rs)) -> SDot ((f lb rb), zipwith ls rs f)
        | (SDot (lb, ls), SInd (rb, rs)) -> SDot ((f lb rb), zipwith ls rs f)
        | (SInd (lb, ls), SDot (rb, rs)) -> SDot ((f lb rb), zipwith ls rs f)
        | (SEnd, SEnd) -> SEnd
        | _ -> failwith "Tried to zip sequences of different lengths."