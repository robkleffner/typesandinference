﻿namespace Inference.Dots

module Types =
    
    open Inference.Common.Common
    open Kinds

    type Type =
        | TVar of name: string * kind: Kind * indexes: int list
        | TCon of name: string * kind: Kind
        | TApp of left: Type * right: Type
        | TSeq of seq: DotSeq.DotSeq<Type>

        override this.ToString () =
            match this with
            | TVar (n, k, inds) -> n
            | TCon (n, k) -> n
            | TApp (l, r) ->
                match r with
                | TApp _ -> $"{l.ToString()} ({r.ToString()})"
                | _ -> $"{l.ToString()} {r.ToString()}"
            | TSeq u -> $"<{u}>"

    type TypeScheme = { Quantified: List<(string * Kind)>; Body: Type }


    // Functional constructors
    let typeVar n k is = TVar (n, k, is)
    let typeCon n k = TCon (n, k)
    let typeApp l r = TApp (l, r)
    let typeSeq s = TSeq s
    
    let schemeType quantified body = { Quantified = quantified; Body = body }
    
    let funType inp outp = typeApp (typeApp (typeCon "->" (KArrow (KData, (KArrow (KData, KData))))) inp) outp


    // Type sequence utilities
    let isSeq t =
        match t with
        | TSeq _ -> true
        | _ -> false

    let isInd t =
        match t with
        | TSeq _ -> false
        | _ -> true

    let getSeq t =
        match t with
        | TSeq ts -> ts
        | _ -> failwith "Called getSeq on non-TSeq"

    let emptySeqOrInd (t : Type) =
        match t with
        | TSeq (DotSeq.SEnd) -> true
        | TSeq (_) -> false
        | _ -> true


    // Free variable computations
    let rec typeFree t =
        match t with
        | TVar (n, _, _) -> Set.singleton n
        | TCon _ -> Set.empty
        | TApp (l, r) -> Set.union (typeFree l) (typeFree r)
        | TSeq s -> DotSeq.toList s |> List.map typeFree |> Set.unionMany
    
    let schemeFree s = Set.difference (typeFree s.Body) (Set.ofList (List.map fst s.Quantified))


    // Kind computations
    let rec typeKind(t : Type) =
        match t with
        | TVar(_, k, _) -> k
        | TCon(_, k) -> k
        | TApp(l, r) ->
            match typeKind l with
            | KArrow(al, ar) -> if (al = typeKind r) then ar else failwith "Kind error: tried to apply type constructor to arg of wrong kind"
            | _ -> failwith "Kind error: type constructor requires arrow kind"
        | TSeq ts -> seqTypeKind ts |> KSeq
    and seqTypeKind (ts : DotSeq.DotSeq<Type>) =
        match ts with
        //| SEnd -> KData // handled by the next branch, but its presence keeps the compiler from whining
        | ts when DotSeq.all isInd ts -> KData
        | ts when DotSeq.any isSeq ts && DotSeq.any isInd ts -> failwith "Tried to get kind of sequence with mixed data and nested sequences."
        | ts -> DotSeq.map typeKind ts |> maxKindSeq
    and maxKindSeq (ks : DotSeq.DotSeq<Kind>) =
        match ks with
        | DotSeq.SEnd -> failwith "Called maxKindSeq on an empty sequence."
        | DotSeq.SInd (k, DotSeq.SEnd) -> k
        | DotSeq.SDot (k, DotSeq.SEnd) -> k
        | DotSeq.SInd (k, ks) -> maxSeq k (maxKindSeq ks)
        | DotSeq.SDot (k, ks) -> maxSeq k (maxKindSeq ks)


    // Sequence and individual substitution computations
    let rec getsub (inds : int list) (sub : Type) =
        match (inds, sub) with
        | ([], s) -> s
        | ([i], TSeq s) -> DotSeq.at i s
        | (i :: is, TSeq s) -> getsub is (DotSeq.at i s)
        | _ -> failwith "Tried to index a non-sequence type."

    let zipExtendRest (ts : Type) =
        match ts with
        | TSeq (DotSeq.SInd (_, rs)) -> TSeq rs
        | TSeq (DotSeq.SDot (_, rs)) -> TSeq rs
        | TSeq (DotSeq.SEnd) -> failwith "Tried to zipExtendRest an empty sequence."
        | any -> any

    let rec copyAt (i : int) (t : Type) =
        match t with
        | TVar (n, k, inds) -> TVar (n, k, List.append inds [i])
        | TCon _ -> t
        | TApp (l, r) -> TApp (copyAt i l, copyAt i r)
        | TSeq ts -> DotSeq.map (copyAt i) ts |> TSeq

    let zipExtendHeads (i : int) (ts : Type) =
        match ts with
        | TSeq (DotSeq.SInd (b, _)) -> b
        | TSeq (DotSeq.SDot (b, _)) -> b
        | TSeq (DotSeq.SEnd) -> failwith "Tried to zipExtendHeads an empty sequence."
        | any -> copyAt i any

    let rec dotOrInd (ts : DotSeq.DotSeq<Type>) =
        match ts with
        | DotSeq.SInd (TSeq (DotSeq.SDot _), _) -> DotSeq.SDot
        | DotSeq.SDot (TSeq (DotSeq.SDot _), _) -> DotSeq.SDot
        | DotSeq.SInd (_, rs) -> dotOrInd rs
        | DotSeq.SDot (_, rs) -> dotOrInd rs
        | DotSeq.SEnd -> DotSeq.SInd

    let rec spliceDots (ts : DotSeq.DotSeq<Type>) =
        match ts with
        | DotSeq.SDot (TSeq ts, rs) ->
            if DotSeq.any isSeq ts
            then DotSeq.SDot (TSeq ts, spliceDots rs)
            else DotSeq.append ts (spliceDots rs)
        | DotSeq.SDot (d, rs) -> DotSeq.SDot (d, spliceDots rs)
        | DotSeq.SInd (i, rs) -> DotSeq.SInd (i, spliceDots rs)
        | DotSeq.SEnd -> DotSeq.SEnd

    let rec zipExtend (ts : DotSeq.DotSeq<Type>) =
        let rec zipExtendInc ts i =
            if DotSeq.any isSeq ts
            then if DotSeq.all (fun t -> emptySeqOrInd t) ts
                 then DotSeq.SEnd
                 else if DotSeq.any (fun t -> isSeq t && emptySeqOrInd t) ts
                 then failwith "zipExtend sequences were of different length."
                 else (dotOrInd ts) (TSeq (zipExtend (DotSeq.map (zipExtendHeads i) ts)), zipExtendInc (DotSeq.map zipExtendRest ts) (1 + i))
            else ts

        if DotSeq.all isSeq ts && DotSeq.anyIndInSeq ts
        then DotSeq.map (fun t -> getSeq t |> zipExtend |> TSeq) ts
        else zipExtendInc (spliceDots ts) 0

    let extend (t: Type) (len: int) = [ for i in 0..len-1 do yield copyAt i t ] |> DotSeq.ofList

    let rec fixApp (t : Type) =
        match t with
        | TApp (TSeq ls, TSeq rs) -> DotSeq.zipwith ls rs (fun l r -> TApp (l, r) |> fixApp) |> TSeq
        | TApp (TSeq ls, r) -> DotSeq.zipwith ls (extend r (DotSeq.length ls)) (fun l r -> TApp (l, r) |> fixApp) |> TSeq
        | TApp (l, TSeq rs) ->
            let canApplySeq =
                match typeKind l with
                | KArrow (arg, _) -> arg = typeKind (TSeq rs)
                | _ -> false
            if canApplySeq
            then TApp (l, TSeq rs)
            else DotSeq.zipwith (extend l (DotSeq.length rs)) rs (fun l r -> TApp (l, r) |> fixApp) |> TSeq
        | TApp (l, r) -> TApp (l, r)
        | _ -> failwith "Called fixApp on non TApp"

    let rec typeSubst (target: string) (sub: Type) (rep : Type) =
        match rep with
        | TCon (_, _) -> rep
        | TVar (n, _, inds) -> if n = target then getsub inds sub else rep
        | TApp (l,r) -> TApp (typeSubst target sub l, typeSubst target sub r) |> fixApp
        | TSeq (ts) -> DotSeq.map (typeSubst target sub) ts |> zipExtend |> TSeq

    let applySubstType subst target = Map.fold (fun ty var sub -> typeSubst var sub ty) target subst
    
    let composeSubst subl subr = Map.map (fun _ v -> applySubstType subl v) subr |> mapUnion fst subl
