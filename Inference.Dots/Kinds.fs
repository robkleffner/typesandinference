﻿namespace Inference.Dots

module Kinds =
    
    type Kind =
        | KData
        | KArrow of Kind * Kind
        | KSeq of Kind

    let rec prettyKind(k : Kind) =
        match k with
        | KData -> "*"
        | KArrow(f, a) -> "(" + prettyKind f + " -> " + prettyKind a + ")"
        | KSeq(k) -> "[" + prettyKind k + "]"

    let rec maxSeq (l : Kind) (r : Kind) =
        match (l, r) with
        | (KSeq kl, KSeq kr) -> KSeq (maxSeq kl kr)
        | (KSeq kl, _) -> KSeq kl
        | (_, KSeq kr) -> KSeq kr
        | (l, r) -> if l <> r then failwith "Non-sequence types within a sequence must all have the same kind" else l