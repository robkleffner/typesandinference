﻿namespace Inference.Dots.Test

open Microsoft.VisualStudio.TestTools.UnitTesting
open Inference.Dots.DotSeq
open Inference.Dots.Kinds
open Inference.Dots.Types

[<TestClass>]
type SubstitutionTests () =

    let dvar name = TVar (name, KData, [])
    
    let divar name inds = TVar (name, KData, inds)

    let allDotSeq ts =
        let rec allDotSeqRec ts =
            match ts with
            | [] -> SEnd
            | t :: ts -> SDot (t, allDotSeqRec ts)
        allDotSeqRec ts |> TSeq

    let lastDotSeq ts =
        let rec lastDotSeqRec ts =
            match ts with
            | [] -> SEnd
            | [t] -> SDot (t, SEnd)
            | t :: ts -> SInd (t, lastDotSeqRec ts)
        lastDotSeqRec ts |> TSeq

    let noDotSeq ts =
        let rec noDotSeqRec ts =
            match ts with
            | [] -> SEnd
            | t :: ts -> SInd (t, noDotSeqRec ts)
        noDotSeqRec ts |> TSeq

    let lty var = TApp (TCon ("List", KArrow(KData, KData)), TVar (var, KData, []))
    let lsty sub = TApp (TCon ("List", KArrow(KData, KData)), sub)
    let idMap var = TApp (TApp (TCon ("Map", KArrow(KData, KArrow(KData, KData))), TVar (var, KData, [])), TVar (var, KData, []))
    let pidMap var inds = TApp (TApp (TCon ("Map", KArrow(KData, KArrow(KData, KData))), TVar (var, KData, inds)), TVar (var, KData, inds))
    let tyMap ty1 ty2 = TApp (TApp (TCon ("Map", KArrow(KData, KArrow(KData, KData))), ty1), ty2)
    let tty sub = TApp (TCon ("Tupl", KArrow(KSeq(KData), KData)), sub)

    [<TestMethod>]
    member this.NoSubstitution () =
        Assert.AreEqual(
            lty "c",
            typeSubst "b" (lty "a") (lty "c"))

    [<TestMethod>]
    member this.SimpleSubstitution () =
        Assert.AreEqual(
            TApp (TCon ("List", KArrow(KData, KData)), (lty "a")),
            typeSubst "b" (lty "a") (lty "b"))

    [<TestMethod>]
    member this.SimpleSequenceSub () =
        Assert.AreEqual(
            (lastDotSeq [dvar "a"; dvar "c"; dvar "d"]),
            typeSubst "b" (lastDotSeq [dvar "a"; dvar "c"; dvar "d"]) (dvar "b"))

    [<TestMethod>]
    member this.IndexSequenceSub () =
        Assert.AreEqual(
            lty "c",
            typeSubst "b" (lastDotSeq [dvar "a"; lty "c"; dvar "d"]) (divar "b" [1]))

    [<TestMethod>]
    member this.ListDottedPartOfExtendFlatTest () =
        Assert.AreEqual(
            lastDotSeq [lty "c"; lty "d"; lty "e"],
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (lastDotSeq [lsty (dvar "b")]))

    [<TestMethod>]
    member this.ListSubstitutionFlatTest () =
        Assert.AreEqual(
            (lastDotSeq [lty "c"; lty "d"; lty "e"]),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (lty "b"))

    [<TestMethod>]
    member this.ListSubstitutionNestedTest () =
        Assert.AreEqual(
            (lastDotSeq [
                (lastDotSeq [lty "c"; lty "d"; lty "e"]);
                (lastDotSeq [lty "f"; lty "g"])]),
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (lty "b"))

    [<TestMethod>]
    member this.MapSubsitutionIdentityFlatTest () =
        Assert.AreEqual(
            (lastDotSeq [idMap "c"; idMap "d"; idMap "e"]),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (idMap "b"))

    [<TestMethod>]
    member this.MapSubsitutionIdentityNestedTest () =
        Assert.AreEqual(
            (lastDotSeq [
                (lastDotSeq [idMap "c"; idMap "d"; idMap "e"]);
                (lastDotSeq [idMap "f"; idMap "g"])]),
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (idMap "b"))

    [<TestMethod>]
    member this.MapPartialExtendNestedTest () =
        Assert.AreEqual(
            lastDotSeq [
                lastDotSeq [idMap "c"; idMap "d"; idMap "e"];
                lastDotSeq [idMap "f"; idMap "g"]],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (lastDotSeq [pidMap "b" [0]; pidMap "b" [1]]))

    [<TestMethod>]
    member this.DotSubstitutionFlat () =
        Assert.AreEqual(
            lastDotSeq [dvar "a"; dvar "a"; dvar "c"; dvar "d"; dvar "e"],
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (lastDotSeq [dvar "a"; dvar "a"; dvar "b"]))

    [<TestMethod>]
    member this.DotSubstitutionNested () =
        Assert.AreEqual(
            lastDotSeq [
                lastDotSeq [divar "a" [0]; divar "a" [0]; dvar "c"; dvar "d"; dvar "e"];
                lastDotSeq [divar "a" [1]; divar "a" [1]; dvar "f"; dvar "g"]],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (lastDotSeq [dvar "a"; dvar "a"; dvar "b"]))

    [<TestMethod>]
    member this.IndSubstitutionFlat () =
        Assert.AreEqual(
            lastDotSeq [
                lastDotSeq [dvar "c"; dvar "c"; divar "a" [0]];
                lastDotSeq [dvar "d"; dvar "d"; divar "a" [1]];
                lastDotSeq [dvar "e"; dvar "e"; divar "a" [2]]],
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (lastDotSeq [dvar "b"; dvar "b"; dvar "a"]))

    [<TestMethod>]
    member this.IndSubstitutionNested () =
        Assert.AreEqual(
            lastDotSeq [
                lastDotSeq [lastDotSeq [dvar "c"; dvar "c"; divar "a" [0; 0]];
                            lastDotSeq [dvar "d"; dvar "d"; divar "a" [0; 1]];
                            lastDotSeq [dvar "e"; dvar "e"; divar "a" [0; 2]]];
                lastDotSeq [lastDotSeq [dvar "f"; dvar "f"; divar "a" [1; 0]];
                            lastDotSeq [dvar "g"; dvar "g"; divar "a" [1; 1]]]],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (lastDotSeq [dvar "b"; dvar "b"; dvar "a"]))

    [<TestMethod>]
    member this.TupleFlatTest () =
        Assert.AreEqual(
            tty (lastDotSeq [lty "c"; lty "d"; lty "e"]),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (tty (lastDotSeq [lty "b"])))

    [<TestMethod>]
    member this.TupleNestedTest () =
        Assert.AreEqual(
            lastDotSeq [
                tty (lastDotSeq [lty "c"; lty "d"; lty "e"]);
                tty (lastDotSeq [lty "f"; lty "g"])],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]]) (tty (lastDotSeq [lty "b"])))

    [<TestMethod>]
    member this.TupleNonSubIndElemFlatTest () =
        Assert.AreEqual(
            lastDotSeq [tty (noDotSeq [dvar "c"; dvar "c"; divar "a" [0]]); tty (noDotSeq [dvar "d"; dvar "d"; divar "a" [1]]); tty (noDotSeq [dvar "e"; dvar "e"; divar "a" [2]])],
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (tty (noDotSeq [dvar "b"; dvar "b"; dvar "a"])))

    [<TestMethod>]
    member this.TupleNonSubDotElemFlatTest () =
        Assert.AreEqual(
            lastDotSeq [tty (lastDotSeq [dvar "c"; dvar "c"; divar "a" [0]]); tty (lastDotSeq [dvar "d"; dvar "d"; divar "a" [1]]); tty (lastDotSeq [dvar "e"; dvar "e"; divar "a" [2]])],
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (tty (lastDotSeq [dvar "b"; dvar "b"; dvar "a"])))

    [<TestMethod>]
    member this.TuplePieceFlatTest () =
        Assert.AreEqual(
            tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"])])),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]) (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [dvar "b"])]))))

    [<TestMethod>]
    member this.TuplePieceNestedTest () =
        Assert.AreEqual(
            tyMap (tty (lastDotSeq [dvar "a"]))
                  (tty (lastDotSeq [tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]);
                                    tty (lastDotSeq [dvar "f"; dvar "g"])])),
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]])
                (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [dvar "b"])]))))

    [<TestMethod>]
    member this.TuplePieceAlreadyExtendFlatTest () =
        Assert.AreEqual(
            tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"])])),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"])
                (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [divar "b" [0]; divar "b" [1]; divar "b" [2]])]))))

    [<TestMethod>]
    member this.TuplePiecePartialExtendNestedTest () =
        Assert.AreEqual(
            tyMap (tty (lastDotSeq [dvar "a"]))
                  (tty (lastDotSeq [tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]);
                                    tty (lastDotSeq [dvar "f"; dvar "g"])])),
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]])
                (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [tty (lastDotSeq [divar "b" [0]]); tty (lastDotSeq [divar "b" [1]])]))))

    [<TestMethod>]
    member this.TupleNestedAllDots () =
        Assert.AreEqual(
            tty
                (allDotSeq [tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]);
                            tty (lastDotSeq [dvar "f"; dvar "g"])]),
            typeSubst "b" (allDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]])
                (tty (lastDotSeq [tty (lastDotSeq [dvar "b"])])))

    [<TestMethod>]
    member this.TupleMapFlatTest () =
        Assert.AreEqual(
            tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"])),
            typeSubst "b" (lastDotSeq [dvar "c"; dvar "d"; dvar "e"])
                (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [dvar "b"]))))

    [<TestMethod>]
    member this.TupleMapNestedTest () =
        Assert.AreEqual(
            lastDotSeq [
                tyMap (tty (lastDotSeq [divar "a" [0]])) (tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]));
                tyMap (tty (lastDotSeq [divar "a" [1]])) (tty (lastDotSeq [dvar "f"; dvar "g"]))],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]])
                (tyMap (tty (lastDotSeq [dvar "a"])) (tty (lastDotSeq [dvar "b"]))))

    [<TestMethod>]
    member this.TupleMapKeyDoubleSeqNestedTest () =
        Assert.AreEqual(
            lastDotSeq [
                tyMap (tty (lastDotSeq [tty (lastDotSeq [divar "a" [0]])])) (tty (lastDotSeq [dvar "c"; dvar "d"; dvar "e"]));
                tyMap (tty (lastDotSeq [tty (lastDotSeq [divar "a" [1]])])) (tty (lastDotSeq [dvar "f"; dvar "g"]))],
            typeSubst "b" (lastDotSeq [lastDotSeq [dvar "c"; dvar "d"; dvar "e"]; lastDotSeq [dvar "f"; dvar "g"]])
                (tyMap (tty (lastDotSeq [tty (lastDotSeq [dvar "a"])])) (tty (lastDotSeq [dvar "b"]))))

    [<TestMethod>]
    member this.EmptySubInTuple () =
        Assert.AreEqual(
            tty (noDotSeq []),
            typeSubst "b" (noDotSeq []) (tty (lastDotSeq [dvar "b"])))

    [<TestMethod>]
    member this.NestedEmptySubInTuple () =
        Assert.AreEqual(
            lastDotSeq [tty (noDotSeq []); tty (noDotSeq [])],
            typeSubst "b" (lastDotSeq [noDotSeq []; noDotSeq []]) (tty (lastDotSeq [dvar "b"])))

    [<TestMethod>]
    member this.PartialNestedEmptySubInTuple () =
        Assert.AreEqual(
            lastDotSeq [tty (noDotSeq [divar "a" [0]; dvar "c"; dvar "d"; dvar "e"]); tty (noDotSeq [divar "a" [1]])],
            typeSubst "b" (lastDotSeq [noDotSeq [dvar "c"; dvar "d"; dvar "e"]; noDotSeq []]) (tty (lastDotSeq [dvar "a"; dvar "b"])))