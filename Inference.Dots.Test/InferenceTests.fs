﻿namespace Inference.Dots.Test

open Microsoft.VisualStudio.TestTools.UnitTesting
open Inference.Dots.DotSeq
open Inference.Dots.Kinds
open Inference.Dots.Types
open Inference.Dots.Terms
open Inference.Dots.Environment
open Inference.Dots.Inference

[<TestClass>]
type BasicInferenceTests () =

    [<TestMethod>]
    member this.IdentityFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bvar "x")),
            funType (typeVar "t0" KData []) (typeVar "t0" KData []))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "y"))),
            funType (typeVar "t0" KData []) (funType (typeVar "t1" KData []) (typeVar "t1" KData [])))

    [<TestMethod>]
    member this.ConstFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "x"))),
            funType (typeVar "t0" KData []) (funType (typeVar "t1" KData []) (typeVar "t0" KData [])))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "y")) (bvar "x"))),
            funType (typeVar "t0" KData []) (typeVar "t0" KData []))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "x")) (bvar "x"))),
            funType (typeVar "t0" KData []) (typeVar "t0" KData []))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            inferFromEmpty (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))),
            funType (typeVar "t2" KData []) (typeVar "t2" KData []))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            inferFromEmpty (blet "s" (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")),
            funType (typeVar "t4" KData []) (typeVar "t4" KData []))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bapp (bvar "x") (bvar "y")))),
            funType (funType (typeVar "t1" KData []) (typeVar "t2" KData [])) (funType (typeVar "t1" KData []) (typeVar "t2" KData [])))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)

[<TestClass>]
type SequenceInferenceTests () =
    
    let initialEnv = [
        termVarBind "map"
            (schemeType [("a", KData);("b", KData)]
                (funType (funType (typeVar "a" KData []) (typeVar "b" KData []))
                    (funType (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData))) (typeSeq (SDot (typeVar "a" KData [], SEnd))))
                        (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData))) (typeSeq (SDot (typeVar "b" KData [], SEnd)))))));
        termVarBind "any"
            (schemeType [("a", KData)]
                (funType (funType (typeVar "a" KData []) (typeCon "Bool" KData))
                    (funType (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData))) (typeSeq (SDot (typeVar "a" KData [], SEnd))))
                        (typeCon "Bool" KData))));
        termVarBind "all"
            (schemeType [("a", KData)]
                (funType (funType (typeVar "a" KData []) (typeCon "Bool" KData))
                    (funType (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData))) (typeSeq (SDot (typeVar "a" KData [], SEnd))))
                        (typeCon "Bool" KData))));
        termVarBind "length"
            (schemeType [("a", KData)] (funType (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData [])) (typeCon "Int" KData)));
        termVarBind "intLit" (schemeType [] (typeCon "Int" KData));
        termVarBind "boolLit" (schemeType [] (typeCon "Bool" KData));
        termVarBind "charLit" (schemeType [] (typeCon "Char" KData));
        termVarBind "nil" (schemeType [("a", KData)] (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData [])));
        termVarBind "fix"
            (schemeType [("a", KData); ("b", KData)]
                (funType (funType (funType (typeVar "a" KData []) (typeVar "b" KData []))
                    (funType (typeVar "a" KData []) (typeVar "b" KData [])))
                        (funType (typeVar "a" KData []) (typeVar "b" KData []))));
        termVarBind "empty"
            (schemeType [("a", KData)]
                (funType (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData []))
                    (typeCon "Bool" KData)));
        termVarBind "cons"
            (schemeType [("a", KData)]
                (funType (typeVar "a" KData [])
                    (funType (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData []))
                        (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData [])))));
        termVarBind "head"
            (schemeType [("a", KData)]
                (funType (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData []))
                    (typeVar "a" KData [])));
        termVarBind "rest"
            (schemeType [("a", KData)]
                (funType (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData []))
                    (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a" KData []))));
        termVarBind "if"
            (schemeType [("a", KData); ("b", KData); ("c", KData)]
                (funType (typeCon "Bool" KData)
                    (funType (typeVar "a" KData [])
                        (funType (typeVar "a" KData [])
                            (typeVar "a" KData [])))))
    ]

    let termIf cond thenBr elseBr =
        bapp (bapp (bapp (bvar "if") cond) thenBr) elseBr

    [<TestMethod>]
    member this.TupleOfListLen () =
        Assert.AreEqual(
            inferFrom initialEnv (babs "tup" (bapp (bapp (bvar "map") (bvar "length")) (bvar "tup"))),
            funType
                (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                         (typeSeq (SDot (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a3" KData []), SEnd))))
                (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                         (typeSeq (SDot (typeCon "Int" KData, SEnd)))))

    [<TestMethod>]
    member this.VariableFunAsArg () =
        Assert.ThrowsException(fun () ->
            inferFrom initialEnv (babs "f" (bapp (bapp (bvar "map") (bvar "f")) (bapp (bapp (bapp (BTuple 3) (bvar "intLit")) (bvar "boolLit")) (bvar "charLit")))) |> ignore) |> ignore
        Assert.IsTrue(true)

    [<TestMethod>]
    member this.ZipN () =
        Assert.AreEqual(
            inferFrom initialEnv
                (blet "zip"
                    (babs "f"
                        (babs "as"
                            (termIf (bapp (bapp (bvar "any") (bvar "empty")) (bvar "as"))
                                (bvar "nil")
                                (bapp
                                    (bapp (bvar "cons") (bapp (bapp (bvar "map") (bvar "head")) (bvar "as")))
                                    (bapp (bvar "f") (bapp (bapp (bvar "map") (bvar "rest")) (bvar "as")))))))
                    (bapp (bvar "fix") (bvar "zip"))),
            funType
                (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                         (typeSeq (SDot (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a29" KData []), SEnd))))
                (typeApp (typeCon "List" (KArrow (KData, KData)))
                    (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                        (typeSeq (SDot (typeVar "a29" KData [], SEnd))))))

    [<TestMethod>]
    member this.FoldLeft () =
        Assert.AreEqual(
            inferFrom initialEnv
                (blet "foldl"
                    (babs "f"
                        (babs "g"
                            (babs "acc"
                                (babs "as"
                                    (termIf (bapp (bapp (bvar "any") (bvar "empty")) (bvar "as"))
                                        (bvar "acc")
                                        (bapp 
                                            (bapp (bapp (bvar "f") (bvar "g"))
                                                (bapp (bapp (bvar "g") (bvar "acc")) (bapp (bapp (bvar "map") (bvar "head")) (bvar "as"))))
                                            (bapp (bapp (bvar "map") (bvar "rest")) (bvar "as"))))))))
                    (bapp (bvar "fix" ) (bvar "foldl"))),
            funType
                (funType (typeVar "t31" KData [])
                    (funType (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                        (typeSeq (SDot (typeVar "a32" KData [], SEnd))))
                        (typeVar "t31" KData [])))
                (funType (typeVar "t31" KData [])
                    (funType (typeApp (typeCon "Tuple" (KArrow (KSeq KData, KData)))
                             (typeSeq (SDot (typeApp (typeCon "List" (KArrow (KData, KData))) (typeVar "a32" KData []), SEnd))))
                        (typeVar "t31" KData []))))