namespace Inference.Basic.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

open Inference.Basic.Kinds
open Inference.Basic.Terms
open Inference.Basic.Types
open Inference.Basic.Inference

[<TestClass>]
type BasicTests () =

    [<TestMethod>]
    member this.IdentityFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bvar "x")),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "y"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t1" KData)))

    [<TestMethod>]
    member this.ConstFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "x"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "y")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "x")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            inferFromEmpty (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))),
            funType (typeVar "t2" KData) (typeVar "t2" KData))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            inferFromEmpty (blet "s" (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")),
            funType (typeVar "t4" KData) (typeVar "t4" KData))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bapp (bvar "x") (bvar "y")))),
            funType (funType (typeVar "t1" KData) (typeVar "t2" KData)) (funType (typeVar "t1" KData) (typeVar "t2" KData)))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)
