namespace Inference.Units.FSharp.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

open Inference.Units.Kinds
open Inference.Units.Terms
open Inference.Units.Types
open Inference.Units.Environment
open Inference.Units.Inference

[<TestClass>]
type BasicTests () =

    [<TestMethod>]
    member this.IdentityFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bvar "x")),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "y"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t1" KData)))

    [<TestMethod>]
    member this.ConstFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "x"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "y")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "x")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            inferFromEmpty (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))),
            funType (typeVar "t2" KData) (typeVar "t2" KData))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            inferFromEmpty (blet "s" (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")),
            funType (typeVar "t4" KData) (typeVar "t4" KData))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bapp (bvar "x") (bvar "y")))),
            funType (funType (typeVar "t1" KData) (typeVar "t2" KData)) (funType (typeVar "t1" KData) (typeVar "t2" KData)))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)

[<TestClass>]
type UnitTests () =
    
    let initialEnv = [
        termVarBind "zero" (schemeType [("x", KUnit)] (typeFloat (typeVar "x" KUnit)));
        termVarBind "one" (schemeType [] (typeFloat (typeUnit Map.empty Map.empty)));
        termVarBind "mass" (schemeType [] (typeFloat (TUnit (simpleConstUnit "kg"))));
        termVarBind "time" (schemeType [] (typeFloat (TUnit (simpleConstUnit "sec"))));
        termVarBind "mul"
            (schemeType [("a", KUnit); ("b", KUnit)]
                (funType (typeFloat (typeVar "a" KUnit))
                    (funType (typeFloat (typeVar "b" KUnit))
                        (typeFloat (typeUnit (Map.ofList [("a", 1);("b",1)]) Map.empty)))));
        termVarBind "plus"
            (schemeType [("a", KUnit)]
                (funType (typeFloat (typeVar "a" KUnit))
                    (funType (typeFloat (typeVar "a" KUnit))
                        (typeFloat (typeVar "a" KUnit)))));
        termVarBind "div"
            (schemeType [("a", KUnit); ("b", KUnit)]
                (funType (typeFloat (typeUnit (Map.ofList [("a", 1);("b",1)]) Map.empty))
                    (funType (typeFloat (typeVar "a" KUnit))
                        (typeFloat (typeVar "b" KUnit)))));
        termVarBind "pair"
            (schemeType [("a", KUnit); ("b", KUnit)]
                (funType (typeFloat (typeVar "a" KUnit))
                    (funType (typeFloat (typeVar "b" KUnit))
                        (funType (typeFloat (typeVar "a" KUnit))
                            (typeFloat (typeVar "b" KUnit))))));
    ]

    [<TestMethod>]
    member this.FloatConst () =
        Assert.AreEqual(
            inferFrom initialEnv (bvar "zero"),
            typeFloat (typeVar "x0" KUnit))
    
    [<TestMethod>]
    member this.MulUnit () =
        Assert.AreEqual(
            typeFloat (typeUnit Map.empty (Map.add "kg" 2 Map.empty)),
            inferFrom initialEnv (bapp (bapp (bvar "mul") (bvar "mass")) (bvar "mass")))

    [<TestMethod>]
    member this.AddUnit () =
        Assert.AreEqual(
            inferFrom initialEnv (babs "a" (babs "b" (bapp (bapp (bvar "plus") (bvar "a")) (bvar "b")))),
            funType (typeFloat (typeVar "a2" KUnit)) (funType (typeFloat (typeVar "a2" KUnit)) (typeFloat (typeVar "a2" KUnit))))

    [<TestMethod>]
    member this.DivUnit () =
        Assert.AreEqual(
            inferFrom initialEnv
                (babs "a"
                    (blet "d"
                        (bapp (bvar "div") (bvar "a"))
                        (bapp (bapp (bvar "pair") (bapp (bvar "d") (bvar "mass"))) (bapp (bvar "d") (bvar "time"))))),
            funType
                (typeFloat (typeVar "h4" KUnit))
                (funType
                    (typeFloat (typeUnit (Map.ofList [("h4", 1)]) (Map.ofList [("kg", -1)])))
                    (typeFloat (typeUnit (Map.ofList [("h4", 1)]) (Map.ofList [("sec", -1)])))))

    [<TestMethod>]
    member this.LetDivUnit () =
        Assert.AreEqual(
            inferFrom initialEnv
                (blet "recip"
                    (bapp (bvar "div") (bvar "one"))
                    (bapp (bapp (bvar "pair") (bapp (bvar "recip") (bvar "mass"))) (bapp (bvar "recip") (bvar "time")))),
            funType
                (typeFloat (typeUnit Map.empty (Map.ofList [("kg", -1)])))
                (typeFloat (typeUnit Map.empty (Map.ofList [("sec", -1)]))))