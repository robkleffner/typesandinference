namespace Inference.Typeclasses.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

open Inference.Typeclasses.Kinds
open Inference.Typeclasses.Terms
open Inference.Typeclasses.Types
open Inference.Typeclasses.Environment
open Inference.Typeclasses.Inference

[<TestClass>]
type BasicTests () =

    [<TestMethod>]
    member this.IdentityFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bvar "x")),
            qualType [] (funType (typeVar "t0" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "y"))),
            qualType [] (funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t1" KData))))

    [<TestMethod>]
    member this.ConstFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "x"))),
            qualType [] (funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t0" KData))))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "y")) (bvar "x"))),
            qualType [] (funType (typeVar "t0" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "x")) (bvar "x"))),
            qualType [] (funType (typeVar "t0" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            inferFromEmpty (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))),
            qualType [] (funType (typeVar "t2" KData) (typeVar "t2" KData)))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            inferFromEmpty (blet "s" (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")),
            qualType [] (funType (typeVar "t4" KData) (typeVar "t4" KData)))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bapp (bvar "x") (bvar "y")))),
            qualType [] (funType (funType (typeVar "t1" KData) (typeVar "t2" KData)) (funType (typeVar "t1" KData) (typeVar "t2" KData))))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)

[<TestClass>]
type QualifiedTests () =
    
    [<TestMethod>]
    member this.BasicTypeClasses () =
        let initialEnv = [
            typeClass "Eq" [
                schemeType [] (qualType [] (typeCon "int" KData));
                schemeType [("a", KData)] (qualType [predType "Eq" (typeVar "a" KData)] (typeApp (typeCon "[]" (KArrow (KData, KData))) (typeVar "a" KData)))];
            typeClass "Ord" [];
            typeClass "Show" [];
            typeClass "Read" [];
            typeClass "Bounded" [];
            typeClass "Enum" [];
            typeClass "Functor" [];
            typeClass "Monad" [];
            termVarBind "eq" (schemeType [("a", KData)] (qualType [predType "Eq" (typeVar "a" KData)] (funType (typeVar "a" KData) (funType (typeVar "a" KData) (typeCon "bool" KData)))));
            termVarBind "zero" (schemeType [] (qualType [] (typeCon "int" KData)));
            termVarBind "true" (schemeType [] (qualType [] (typeCon "bool" KData)));
            termVarBind "show" (schemeType [("a", KData)] (qualType [predType "Show" (typeVar "a" KData)] (funType (typeVar "a" KData) (typeCon "string" KData))));
            termVarBind "read" (schemeType [("a", KData)] (qualType [predType "Read" (typeVar "a" KData)] (funType (typeCon "string" KData) (typeVar "a" KData))))]

        Assert.AreEqual(
            inferFrom initialEnv (bvar "eq"),
            qualType [predType "Eq" (typeVar "a0" KData)] (funType (typeVar "a0" KData) (funType (typeVar "a0" KData) (typeCon "bool" KData))))

        Assert.AreEqual(
            inferFrom initialEnv (bapp (bvar "eq") (bvar "zero")),
            qualType [] (funType (typeCon "int" KData) (typeCon "bool" KData)))

        Assert.ThrowsException(fun () -> inferFrom initialEnv (bapp (bvar "eq") (bvar "true")) |> ignore) |> ignore

        Assert.ThrowsException(fun () -> inferFrom initialEnv (babs "x" (bapp (bvar "show") (bapp (bvar "read") (bvar "x")))) |> ignore) |> ignore