﻿namespace Inference.Typeclasses

module Inference =

    open Inference.Common.Common;
    open Kinds;
    open Types;
    open Terms;
    open Environment;

    type InferenceState = { Fresh: int seq; Environment: List<EnvironmentEntry> }


    // utilities for working with inference states
    let extractConstraint context =
        List.tryFindIndex (isConstraint) context
        |> Option.map (fun constrInd -> (List.take constrInd context, List.item constrInd context, List.skip (constrInd + 1) context))

    let freshVar (prefix: string) =
        state
            {
            let! inf = get
            let i = Seq.head inf.Fresh
            do! put { Fresh = Seq.tail inf.Fresh; Environment = inf.Environment }
            return prefix.Substring(0, 1) + i.ToString()
            }

    let getEnvironment =
        state
            {
            let! inf = get
            return inf.Environment
            }

    let updateEnvironment f =
        state
            {
            let! inf = get
            do! put { Fresh = inf.Fresh; Environment = f inf.Environment }
            }

    let putEnvironment context = updateEnvironment (constant context)

    let pushEntry entry = updateEnvironment (fun con -> List.append con [entry])

    let addIntro name kind = pushEntry (typeVarIntro name kind)

    let addBinding name ty = pushEntry (termVarBind name ty)

    let popBinding name =
        let nameBinding item =
            match item with
            | ETermVarBind (n, _) when n = name -> true
            | _ -> false

        updateEnvironment (fun context ->
            let last = List.tryFindIndexBack nameBinding context
            match last with
            | Some ind -> removeAt ind context
            | None -> context)


    // constraint solving in context
    let solveFlexFlex left right kind top =
        match top with
        | ETypeVarIntro (n, k) ->
            match (left = n, right = n) with
            | (true, true) -> [top]
            | (true, false) -> [typeVarDef n (typeVar right kind)]
            | (false, true) -> [typeVarDef n (typeVar left kind)]
            | (false, false) -> [flexFlexConstr left right kind; top]
        | ETypeVarDef (n, _) when left = n && right = n ->
            [top]
        | ETypeVarDef (n, def) ->
            [simpleBinaryConstr (typeSubst n def (typeVar left kind)) (typeSubst n def (typeVar right kind)); top]
        | _ ->
            [flexFlexConstr left right kind; top]

    let solveFlexRigid name kind ty deps top =
        match top with
        | ETypeVarDef (n, def) ->
            let constr =
                if (typeFree ty |> Set.add name).Contains n
                then simpleBinaryConstr (typeSubst n def (typeVar name kind)) (typeSubst n def ty)
                else flexRigidConstr name kind ty []
            List.append deps [constr; top]
        | ETypeVarIntro (n, k) ->
            match (name = n, (typeFree ty).Contains(n)) with
            | (true, true) -> failwith "Occurs check failed"
            | (true, false) -> List.append deps [typeVarDef name ty]
            | (false, true) -> [flexRigidConstr name kind ty (top :: deps)]
            | (false, false) -> [flexRigidConstr name kind ty deps; top]
        | _ -> [flexRigidConstr name kind ty deps; top]

    let solveTypeConstraint left right =
        match (left, right) with
        | (TCon (ln, lk), TCon (rn, rk)) when ln = rn && lk = rk -> []
        | (TApp (ll, lr), TApp (rl, rr)) -> [simpleBinaryConstr ll rl; simpleBinaryConstr lr rr]
        | (TVar (ln, lk), TVar (rn, rk)) when lk = rk -> [flexFlexConstr ln rn lk]
        | (TVar (ln, lk), r) when lk = typeKind r -> [flexRigidConstr ln lk r []]
        | (l, TVar (rn, rk)) when rk = typeKind l -> [flexRigidConstr rn rk l []]
        | _ -> failwith "Rigid-rigid mismatch or kinds do not unify"

    let solveOrMoveUp prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        append3 popped (fn top) suffix |> putEnvironment

    let solveConstraint prefix suffix constr =
        match constr with
        | ETypeConstraint (left, right) ->
            append3 prefix (solveTypeConstraint left right) suffix |> putEnvironment
        | EFlexFlexConstraint (left, right, kind) ->
            solveOrMoveUp prefix suffix (solveFlexFlex left right kind)
        | EFlexRigidConstraint (var, kind, right, deps) ->
            solveOrMoveUp prefix suffix (solveFlexRigid var kind right deps)
        | _ -> failwith "Tried to solve a non-constraint context entry"

    let rec solve () =
        state
            {
            let! env = getEnvironment
            match extractConstraint env with
            | Some (prefix, constr, suffix) ->
                do! solveConstraint prefix suffix constr
                do! solve ()
            | None -> do! putEnvironment env
            }


    // inference: from terms to typed terms
    let specialize poly =
        let rec specialLoop quant body =
            state
                {
                match quant with
                | q :: qs ->
                    let! fresh = freshVar (fst q)
                    do! addIntro fresh (snd q)
                    return! specialLoop qs (qualSubst (fst q) (typeVar fresh (snd q)) body)
                | [] -> return body
                }
        specialLoop poly.Quantified poly.Body

    let addInstance name inst env =
        run { Fresh = Seq.initInfinite id; Environment = [] } (
            state
                {
                match findClass name env with
                | Some (_, insts) ->
                    let! freshInst = specialize inst
                    let overlapping = List.filter ((fun i -> i.Body.Head) >> typeOverlap freshInst.Head) insts
                    if List.isEmpty overlapping
                    then return modifyClass name (inst :: insts) env
                    else return failwith $"Instance overlaps with an existing instance in class {name}"
                | _ -> return failwith $"Could not find type class {name}"
                })

    let unify left right =
        state
            {
            do! pushEntry (simpleBinaryConstr left right)
            do! solve ()
            }

    let skimContext =
        state
            {
            let! context = getEnvironment
            let skimmed = List.rev context |> List.takeWhile (isLocalityMark >> not)
            do! putEnvironment (List.rev context |> List.skipWhile (isLocalityMark >> not) |> List.skip 1 |> List.rev)
            return skimmed
            }

    let splitPredicates preds generalized =
        let containsNonGeneral pred = Set.difference (predFree pred) generalized |> Set.isEmpty |> not
        state
            {
            let! env = getEnvironment
            let reduced = contextReduce preds env
            let deferred = List.filter containsNonGeneral reduced
            let general = List.filter (containsNonGeneral >> not) reduced
            return (deferred, general)
            }

    let ambiguousPredicates preds genVars =
        Set.difference (contextFree preds) genVars
        |> Set.isEmpty
        |> not

    let rec infer term =
        state
            {
            match term with
            | BVar v ->
                let! context = getEnvironment
                match getTermVarType v context with
                | Some t -> return! specialize t
                | None -> return failwith "Could not find binding in context"
            | BAbs (p, b) ->
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! addBinding p (schemeType [] (qualType [] (typeVar fresh KData)))
                let! rinf = infer b
                do! popBinding p
                return qualType rinf.Context (funType (typeVar fresh KData) rinf.Head)
            | BApp (l, r) ->
                let! linf = infer l
                let! rinf = infer r
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! unify linf.Head (funType rinf.Head (typeVar fresh KData))
                return qualType (List.append linf.Context rinf.Context) (typeVar fresh KData)
            | BLet (l, e, b) ->
                let! (geninf, gendeferred) = inferGeneralized e
                do! addBinding l geninf
                let! bodyinf = infer b
                do! popBinding l
                return qualType (List.append gendeferred bodyinf.Context) bodyinf.Head
            }
    and inferGeneralized term =
        state
            {
            do! pushEntry ELocalityMark
            let! ungen = infer term
            let! generalized = skimContext
            let genVariables = Set.ofList (List.map typeVarName generalized)
            let! (deferred, reduced) = splitPredicates ungen.Context genVariables
            if ambiguousPredicates reduced genVariables
            then return failwith "Ambiguity detected in qualified constraint resolution"
            else return makeScheme generalized (qualType reduced ungen.Head), deferred
            }

    let inferFrom environment term =
        let (inf, final) = run { Fresh = Seq.initInfinite id; Environment = environment } (infer term)
        let normalized = applyEnvQual (normalizeEnv final.Environment) inf
        if ambiguousPredicates normalized.Context (typeFree normalized.Head)
        then failwith "Ambiguity detected in qualified constraint resolution"
        else qualType (contextReduce normalized.Context final.Environment) normalized.Head

    let inferFromEmpty term = inferFrom [] term