﻿namespace Inference.Typeclasses

module Types =
    
    open Inference.Common.Common
    open Kinds

    type Type =
        | TVar of string * Kind
        | TCon of string * Kind
        | TApp of Type * Type

    type Predicate = { Name: string; Argument: Type }

    type QualifiedType = { Context: List<Predicate>; Head: Type }

    type TypeScheme = { Quantified: List<(string * Kind)>; Body: QualifiedType }


    // Functional constructors
    let typeVar n k = TVar (n, k)
    let typeCon n k = TCon (n, k)
    let typeApp l r = TApp (l, r)

    let predType name arg = { Name = name; Argument = arg }

    let qualType context head = { Context = context; Head = head }

    let schemeType quantified body = { Quantified = quantified; Body = body }

    let funType inp outp = typeApp (typeApp (typeCon "->" (KArrow (KData, KArrow (KData, KData)))) inp) outp


    // Free variable computations
    let rec typeFree t =
        match t with
        | TVar (n, _) -> Set.singleton n
        | TCon _ -> Set.empty
        | TApp (l, r) -> Set.union (typeFree l) (typeFree r)

    let predFree p = typeFree p.Argument

    let contextFree c = List.map predFree c |> Set.unionMany

    let qualFree q = contextFree q.Context |> Set.union (typeFree q.Head)

    let schemeFree s = Set.difference (qualFree s.Body) (Set.ofList (List.map fst s.Quantified))


    // Substitution computations
    let rec typeSubst var sub target =
        match target with
        | TVar (n, _) -> if var = n then sub else target
        | TCon _ -> target
        | TApp (l, r) -> TApp (typeSubst var sub l, typeSubst var sub r)

    let predSubst var sub pred = { Name = pred.Name; Argument = typeSubst var sub pred.Argument }

    let qualSubst var sub qual = { Context = List.map (predSubst var sub) qual.Context; Head = typeSubst var sub qual.Head }

    let applySubstType subst target = Map.fold (fun ty var sub -> typeSubst var sub ty) target subst

    let applySubstPred subst pred = Map.fold (fun pr var sub -> predSubst var sub pr) pred subst

    let applySubstContext subst context = List.map (applySubstPred subst) context

    let composeSubst subl subr = Map.map (fun _ v -> applySubstType subl v) subr |> mapUnion fst subl


    // Kind computations
    let rec typeKind t =
        match t with
        | TVar (_, k) -> k
        | TCon (_, k) -> k
        | TApp (l, r) -> applyKind (typeKind l) (typeKind r)

    let predKind p = typeKind p.Argument


    // Head noraml form computations
    let rec typeHeadNormalForm t =
        match t with
        | TVar _ -> true
        | TCon _ -> false
        | TApp (l, _) -> typeHeadNormalForm l

    let predHeadNoramlForm p = typeHeadNormalForm p.Argument


    // One-way matching of types
    let rec typeMatch l r =
        match (l, r) with
        | (TVar (n, k), r) -> if k = typeKind r then Option.Some (Map.add n r Map.empty) else Option.None
        | (TCon _, TCon _) -> if l = r then Option.Some Map.empty else Option.None
        | (TApp (ll, lr), TApp (rl, rr)) ->
            maybe
                {
                let! lm = typeMatch ll rl
                let! rm = typeMatch lr rr
                let! merged = merge lm rm
                return merged
                }
        | _ -> Option.None

    let isTypeMatch l r =
        match typeMatch l r with
        | Some _ -> true
        | None -> false


    // Unification of types
    let rec typeUnify l r =
        match (l, r) with
        | (_, _) when l = r -> Option.Some (Map.empty)
        | (TVar (nl, kl), r) when kl = typeKind r && not (Set.contains nl (typeFree r)) -> Option.Some (Map.add nl r Map.empty)
        | (l, TVar (nr, kr)) when kr = typeKind l && not (Set.contains nr (typeFree l)) -> Option.Some (Map.add nr l Map.empty)
        | (TApp (ll, lr), TApp (rl, rr)) ->
            maybe
                {
                let! lu = typeUnify ll rl
                let! ru = typeUnify (applySubstType lu lr) (applySubstType lu rr)
                return composeSubst ru lu
                }
        | _ -> Option.None

    let typeOverlap l r =
        match typeUnify l r with
        | Some _ -> true
        | None -> false