﻿namespace Inference.Typeclasses

module Environment =

    open Kinds
    open Types

    type EnvironmentEntry =
        | ETypeVarIntro of string * Kind
        | ETypeVarDef of string * Type
        | ETermVarBind of string * TypeScheme
        | ELocalityMark
        | ETypeClassDecl of className: string * instances: List<TypeScheme>
        | ETypeConstraint of left: Type * right: Type
        | EFlexFlexConstraint of left: string * right: string * kind: Kind
        | EFlexRigidConstraint of var: string * kind: Kind * rigid: Type * dependencies: List<EnvironmentEntry>


    // utilities for working with environment entries
    let typeVarIntro name kind = ETypeVarIntro (name, kind)

    let typeVarDef name def = ETypeVarDef (name, def)

    let termVarBind name ty = ETermVarBind (name, ty)

    let typeClass name insts = ETypeClassDecl (name, insts)

    let simpleBinaryConstr left right = ETypeConstraint (left, right)

    let flexFlexConstr left right kind = EFlexFlexConstraint (left, right, kind)

    let flexRigidConstr var kind rigid deps = EFlexRigidConstraint (var, kind, rigid, deps)

    let isConstraint entry =
        match entry with
        | ETypeConstraint _ -> true
        | EFlexFlexConstraint _ -> true
        | EFlexRigidConstraint _ -> true
        | _ -> false

    let isLocalityMark entry =
        match entry with
        | ELocalityMark -> true
        | _ -> false

    let typeVarName entry =
        match entry with
        | ETypeVarIntro (n, _) -> n
        | ETypeVarDef (n, _) -> n
        | _ -> failwith "Expected a type variable entry"


    // substitution computations
    let applyEntryType t entry =
        match entry with
        | ETypeVarDef (n, def) -> typeSubst n def t
        | _ -> t

    let applyEnvType env t = List.fold applyEntryType t env

    let applyEnvPred env p = { Name = p.Name; Argument = applyEnvType env p.Argument }

    let applyEnvQual env q = { Context = List.map (applyEnvPred env) q.Context; Head = applyEnvType env q.Head }


    // type class utilities
    let rec findClass name env =
        match env with
        | ETypeClassDecl (n, insts) :: _ when n = name -> Some (n, insts)
        | _ :: es -> findClass name es
        | [] -> None

    let addClass name env =
        match findClass name env with
        | Some _ -> failwith $"Typeclass {name} already exists in environment"
        | None -> ETypeClassDecl (name, []) :: env

    let rec modifyClass name insts env =
        match env with
        | ETypeClassDecl (n, is) :: es when n = name -> ETypeClassDecl (name, insts) :: es
        | e :: es -> e :: modifyClass name insts es
        | [] -> []


    // predicate head normal form
    let getInstanceSubgoals pred env =
        match findClass pred.Name env with
        | Some (_, insts) ->
            let matching = List.filter (fun i -> isTypeMatch i.Body.Head pred.Argument) insts
            if List.isEmpty matching
            then None
            else
                let first = List.head matching
                typeMatch first.Body.Head pred.Argument
                |> Option.map (fun subst -> applySubstContext subst first.Body.Context)
        | None -> failwith "Could not find type class"

    let rec toHeadNormalForm pred env =
        if predHeadNoramlForm pred
        then [pred]
        else
            match getInstanceSubgoals pred env with
            | Some subgoals -> [for sub in subgoals do yield toHeadNormalForm sub env] |> List.concat
            | None -> failwith "Context reduction failed"


    // context operations
    let rec predEntails pred env =
        match getInstanceSubgoals pred env with
        | Some subgoals -> List.forall (fun sub -> predEntails sub env) subgoals
        | None -> false

    let contextToHeadNormalForm context env = List.map (fun p -> toHeadNormalForm p env) context |> List.concat

    let contextSimplify env context =
        let mutable simplified = []
        let mutable remaining = context
        while not (List.isEmpty remaining) do
            let test :: rest = remaining
            if not (predEntails test env)
            then simplified <- test :: simplified
            remaining <- rest
        simplified

    let contextReduce context env = contextToHeadNormalForm context env |> contextSimplify env


    // accessors over contexts
    let rec getTermVarType var env =
        match env with
        | ETermVarBind (n, t) :: _ when n = var -> Option.Some t
        | _ :: cs -> getTermVarType var cs
        | [] -> Option.None

    
    // other utilities
    let makeScheme generalized ty =
        let mutable body = ty
        let mutable quantified = List.empty
        for g in generalized do
            match g with
            | ETypeVarIntro (n, k) -> quantified <- (n, k) :: quantified
            | ETypeVarDef (n, d) -> body <- qualSubst n d body
            | _ -> failwith "Unexpected entry in skimmed context"
        schemeType quantified body

    let normalizeEntry env entry =
        match entry with
        | ETypeVarDef (n, t) -> ETypeVarDef (n, applyEnvType env t)
        | _ -> entry

    let normalizeEnv = List.fold (fun prev next -> normalizeEntry prev next :: prev) []