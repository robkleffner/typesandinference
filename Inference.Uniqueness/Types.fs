﻿namespace Inference.Uniqueness

open System.Diagnostics

module Types =

    open Inference.Common.Common
    open Kinds

    [<DebuggerDisplay("{ToString()}")>]
    type Type =
        | TVar of string * Kind
        | TCon of string * Kind
        | TAttr of Boolean.Equation
        | TApp of Type * Type
        override this.ToString () = $"{this.RecToString()} : {this.Kind()}"

        member this.RecToString () =
            match this with
            | TVar (n, k) -> n
            | TCon (n, k) -> n
            | TApp (l, r) ->
                match r with
                | TApp _ -> $"{l.RecToString()} ({r.RecToString()})"
                | _ -> $"{l.RecToString()} {r.RecToString()}"
            | TAttr u -> $"<{u}>"

        member this.Kind () =
            match this with
            | TVar (_, k) -> k
            | TCon (_, k) -> k
            | TApp (l, r) -> applyKind (l.Kind()) (r.Kind())
            | TAttr _ -> KAttr

    type TypeScheme = { Quantified: List<(string * Kind)>; Body: Type }


    // Functional constructors
    let typeVar n k = TVar (n, k)
    let typeCon n k = TCon (n, k)
    let typeApp l r = TApp (l, r)

    let schemeType quantified body = { Quantified = quantified; Body = body }

    let attrType ty attr = typeApp ty (TAttr attr)

    let funType shared inp outp = typeApp (typeApp (typeApp (typeCon "->" (KArrow (KData, (KArrow (KData, KArrow (KAttr, KData)))))) inp) outp) shared

    let sharedAttr = TAttr Boolean.BFalse

    let toBooleanEqn ty =
        match ty with
        | TAttr eqn -> eqn
        | TVar (n, KAttr) -> Boolean.BVar n
        | _ -> failwith "Tried to convert a non-Boolean equation type to a Boolean equation"

    let attrsToDisjunction attrs =
        List.map toBooleanEqn attrs
        |> List.fold (fun eqn tm -> Boolean.BOr (eqn, tm)) Boolean.BFalse
        |> Boolean.partialEval
        |> TAttr


    // Free variable computations
    let rec typeFree t =
        match t with
        | TVar (n, _) -> Set.singleton n
        | TCon _ -> Set.empty
        | TApp (l, r) -> Set.union (typeFree l) (typeFree r)
        | TAttr eqn -> Boolean.free eqn

    let schemeFree s = Set.difference (typeFree s.Body) (Set.ofList (List.map fst s.Quantified))


    // Substitution computations
    let rec typeSubst var sub target =
        match target with
        | TVar (n, _) -> if var = n then sub else target
        | TCon _ -> target
        | TApp (l, r) -> TApp (typeSubst var sub l, typeSubst var sub r)
        | TAttr b when (Boolean.free b).Contains var -> TAttr (Boolean.substituteAndEval var (toBooleanEqn sub) b)
        | TAttr _ -> target

    let applySubstType subst target = Map.fold (fun ty var sub -> typeSubst var sub ty) target subst

    let composeSubst subl subr = Map.map (fun _ v -> applySubstType subl v) subr |> mapUnion fst subl


    // Kind computations
    let rec typeKind (t : Type) = t.Kind()