﻿namespace Inference.Uniqueness

module Terms =
    
    type Term =
        | BVar of string
        | BAbs of string * bool * Term
        | BApp of Term * Term
        | BLet of string * bool * Term * Term

    let bvar name = BVar name

    let babs param shared tm = BAbs (param, shared, tm)

    let bapp left right = BApp (left, right)

    let blet name shared named exp = BLet (name, shared, named, exp)

    let rec termFree t =
        match t with
        | BVar n -> Set.singleton n
        | BAbs (n, _, b) -> Set.remove n (termFree b)
        | BApp (l, r) -> Set.union (termFree l) (termFree r)
        | BLet (n, _, b, e) -> Set.remove n (termFree e) |> Set.union (termFree b)