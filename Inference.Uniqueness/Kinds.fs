﻿namespace Inference.Uniqueness

open System.Diagnostics

module Kinds =

    [<DebuggerDisplay("{ToString()}")>]
    type Kind =
        | KData
        | KAttr
        | KArrow of Kind * Kind
        override this.ToString () =
            match this with
            | KData -> "*"
            | KAttr -> "?"
            | KArrow (l, r) ->
                match l with
                | KArrow _ -> $"({l}) -> {r}"
                | _ -> $"{l} -> {r}"

    let applyKind kfun karg =
        match kfun with
        | KArrow (input, output) ->
            if input = karg
            then output
            else failwith "Cannot apply kind to input argument: input kind must match argument kind"
        | _ -> failwith "Cannot apply a non-arrow kind"