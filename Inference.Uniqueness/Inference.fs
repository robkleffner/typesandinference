﻿namespace Inference.Uniqueness

module Inference =

    open Inference.Common.Common;
    open Kinds;
    open Types;
    open Terms;
    open Environment;

    type InferenceState = { Fresh: int seq; Environment: List<EnvironmentEntry> }


    // utilities for working with inference states
    let extractConstraint context =
        List.tryFindIndex (isConstraint) context
        |> Option.map (fun constrInd -> (List.take constrInd context, List.item constrInd context, List.skip (constrInd + 1) context))

    let freshVar (prefix: string) =
        state
            {
            let! inf = get
            let i = Seq.head inf.Fresh
            do! put { Fresh = Seq.tail inf.Fresh; Environment = inf.Environment }
            return prefix.Substring(0, 1) + i.ToString()
            }

    let getEnvironment =
        state
            {
            let! inf = get
            return inf.Environment
            }

    let updateEnvironment f =
        state
            {
            let! inf = get
            do! put { Fresh = inf.Fresh; Environment = f inf.Environment }
            }

    let putEnvironment context = updateEnvironment (constant context)

    let pushEntry entry = updateEnvironment (fun con -> List.append con [entry])

    let addIntro name kind = pushEntry (typeVarIntro name kind)

    let addBinding name ty = pushEntry (termVarBind name ty)

    let popBinding name =
        let nameBinding item =
            match item with
            | ETermVarBind (n, _) when n = name -> true
            | _ -> false

        updateEnvironment (List.filter (nameBinding >> not))


    // constraint solving in context
    let solveFlexFlex left right kind top =
        match top with
        | ETypeVarIntro (n, k) ->
            match (left = n, right = n) with
            | (true, true) -> [top]
            | (true, false) -> [typeVarDef n (typeVar right kind)]
            | (false, true) -> [typeVarDef n (typeVar left kind)]
            | (false, false) -> [flexFlexConstr left right kind; top]
        | ETypeVarDef (n, _) when left = n && right = n ->
            [top]
        | ETypeVarDef (n, def) ->
            [simpleBinaryConstr (typeSubst n def (typeVar left kind)) (typeSubst n def (typeVar right kind)); top]
        | _ ->
            [flexFlexConstr left right kind; top]

    let solveFlexRigid name kind ty deps top =
        match top with
        | ETypeVarDef (n, def) ->
            let constr =
                if (typeFree ty |> Set.add name).Contains n
                then simpleBinaryConstr (typeSubst n def (typeVar name kind)) (typeSubst n def ty)
                else flexRigidConstr name kind ty []
            List.append deps [constr; top]
        | ETypeVarIntro (n, k) ->
            match (name = n, (typeFree ty).Contains(n)) with
            | (true, true) -> failwith "Occurs check failed"
            | (true, false) -> List.append deps [typeVarDef name ty]
            | (false, true) -> [flexRigidConstr name kind ty (top :: deps)]
            | (false, false) -> [flexRigidConstr name kind ty deps; top]
        | _ -> [flexRigidConstr name kind ty deps; top]

    let generateFlexRigidConstr var kind rigid =
        match kind with
        | KAttr ->
            match rigid with
            | TAttr a -> binaryAttrConstr (Boolean.BVar var) a
            | _ -> failwith "Kind mismatch"
        | _ -> flexRigidConstr var kind rigid []

    let solveBooleanEquation eqn top =
        match Boolean.unify eqn with
        | Some subst -> [top; EBooleanSubstConstraint (Map.map (fun k v -> TAttr v) subst, [])]
        | None -> failwith "Boolean unification failed"

    let solveBooleanSubstitution subst deps top =
        if Map.isEmpty subst
        then List.append deps [top]
        else
            match top with
            | ETypeVarDef (n, def) ->
                let updatedSubst = Map.remove n (Map.map (fun _ v -> typeSubst n def v) subst)
                if Map.exists (fun _ v -> (typeFree v).Contains n) subst
                then [EBooleanSubstConstraint (updatedSubst, top :: deps)]
                else List.append deps [EBooleanSubstConstraint (updatedSubst, []); top]
            | ETypeVarIntro (n, k) ->
                match (subst.ContainsKey n, Map.exists (fun _ v -> (typeFree v).Contains n) subst) with
                | (true, true) -> List.append deps [typeVarDef n subst.[n]; EBooleanSubstConstraint (Map.remove n subst, [])]
                | (true, false) -> List.append deps [EBooleanSubstConstraint (Map.remove n subst, []); typeVarDef n subst.[n]]
                | (false, true) -> [EBooleanSubstConstraint (subst, top :: deps)]
                | (false, false) -> [EBooleanSubstConstraint (subst, deps); top]
            | _ -> [EBooleanSubstConstraint (subst, deps); top]

    let solveTypeConstraint left right =
        state
            {
            match (left, right) with
            | (TCon (ln, lk), TCon (rn, rk)) when ln = rn && lk = rk ->
                return []
            | (TApp (ll, lr), TApp (rl, rr)) ->
                return [simpleBinaryConstr ll rl; simpleBinaryConstr lr rr]
            | (TVar (ln, lk), TVar (rn, rk)) when lk = rk ->
                return [flexFlexConstr ln rn lk]
            | (TAttr la, TAttr ra) ->
                return [binaryAttrConstr la ra]
            | (TVar (ln, lk), r) when lk = typeKind r ->
                return [generateFlexRigidConstr ln lk r]
            | (l, TVar (rn, rk)) when rk = typeKind l ->
                return [generateFlexRigidConstr rn rk l]
            | _ ->
                return failwith "Rigid-rigid mismatch or kinds do not unify"
            }

    let solveOrMoveUp prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        append3 popped (fn top) suffix |> putEnvironment

    let solveOrMoveUpWithState prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        state
            {
            let! constrs = fn top
            do! append3 popped constrs suffix |> putEnvironment
            }

    let solveConstraint prefix suffix constr =
        match constr with
        | ETypeConstraint (left, right) ->
            state
                {
                let! constrs = solveTypeConstraint left right
                do! append3 prefix constrs suffix |> putEnvironment
                }
        | EFlexFlexConstraint (left, right, kind) ->
            solveOrMoveUp prefix suffix (solveFlexFlex left right kind)
        | EFlexRigidConstraint (var, kind, rigid, deps) ->
            solveOrMoveUp prefix suffix (solveFlexRigid var kind rigid deps)
        | EAttributeConstraint attr ->
            solveOrMoveUp prefix suffix (solveBooleanEquation attr)
        | EBooleanSubstConstraint (subst, deps) ->
            solveOrMoveUp prefix suffix (solveBooleanSubstitution subst deps)
        | _ -> failwith "Tried to solve a non-constraint context entry"

    let rec solve () =
        state
            {
            let! env = getEnvironment
            match extractConstraint env with
            | Some (prefix, constr, suffix) ->
                do! solveConstraint prefix suffix constr
                do! solve ()
            | None -> do! putEnvironment env
            }


    // inference: from terms to typed terms
    let specialize poly =
        let rec specialLoop quant body =
            state
                {
                match quant with
                | q :: qs ->
                    let! fresh = freshVar (fst q)
                    do! addIntro fresh (snd q)
                    return! specialLoop qs (typeSubst (fst q) (typeVar fresh (snd q)) body)
                | [] -> return body
                }
        specialLoop poly.Quantified poly.Body

    let unify left right =
        state
            {
            do! pushEntry (simpleBinaryConstr left right)
            do! solve ()
            }

    let skimContext =
        state
            {
            let! context = getEnvironment
            let skimmed = List.rev context |> List.takeWhile (isLocalityMark >> not)
            do! putEnvironment (List.rev context |> List.skipWhile (isLocalityMark >> not) |> List.skip 1 |> List.rev)
            return skimmed
            }

    let sharedClosure term =
        state
            {
            let! context = getEnvironment
            return List.ofSeq (termFree term)
                |> List.map (fun v -> getTermVarTypeSharing v context)
                |> List.map Option.toList
                |> List.concat
                |> attrsToDisjunction
            }

    let rec infer term =
        state
            {
            match term with
            | BVar v ->
                let! context = getEnvironment
                match getTermVarType v context with
                | Some t -> return! specialize t
                | None -> return failwith "Could not find binding in context"
            | BAbs (p, true, b) ->
                let! fresh = freshVar "t"
                do! addIntro fresh (KArrow (KAttr, KData))
                do! addBinding p (schemeType [] (typeApp (typeVar fresh (KArrow (KAttr, KData))) (TAttr Boolean.BFalse)))
                let! rinf = infer b
                do! popBinding p
                let! shared = sharedClosure b
                return funType shared (typeApp (typeVar fresh (KArrow (KAttr, KData))) (TAttr Boolean.BFalse)) rinf
            | BAbs (p, false, b) ->
                let! freshData = freshVar "t"
                let! freshSharing = freshVar "u"
                do! addIntro freshData (KArrow (KAttr, KData))
                do! addIntro freshSharing KAttr
                do! addBinding p (schemeType [] (typeApp (typeVar freshData (KArrow (KAttr, KData))) (typeVar freshSharing KAttr)))
                let! rinf = infer b
                do! popBinding p
                let! shared = sharedClosure b
                return funType shared (typeApp (typeVar freshData (KArrow (KAttr, KData))) (typeVar freshSharing KAttr)) rinf
            | BApp (l, r) ->
                let! linf = infer l
                let! rinf = infer r
                let! freshRetData = freshVar "t"
                let! freshRetShare = freshVar "u"
                let! freshFunShare = freshVar "u"
                do! addIntro freshRetData (KArrow (KAttr, KData))
                do! addIntro freshRetShare KAttr
                do! addIntro freshFunShare KAttr
                do! unify linf (funType (typeVar freshFunShare KAttr) rinf (typeApp (typeVar freshRetData (KArrow (KAttr, KData))) (typeVar freshRetShare KAttr)))
                return typeApp (typeVar freshRetData (KArrow (KAttr, KData))) (typeVar freshRetShare KAttr)
            | BLet (l, shared, e, b) ->
                let! geninf = inferGeneralized e shared
                do! addBinding l geninf
                let! bodyinf = infer b
                do! popBinding l
                return bodyinf
            }
    and inferGeneralized term shared =
        state
            {
            do! pushEntry ELocalityMark
            let! ungen = infer term
            let ungenShared =
                match ungen with
                | TApp (l, r) when typeKind r = KAttr -> if shared then (TApp (l, TAttr Boolean.BFalse)) else ungen
                | _ -> failwith "Expected type to have form (t:* u:Attr), but it did not"
            let! generalized = skimContext
            return makeScheme generalized ungenShared
            }

    let inferFrom environment term =
        let (inf, final) = run { Fresh = Seq.initInfinite id; Environment = environment } (infer term)
        let normalized = normalizeEnv final.Environment
        applyEnvType normalized inf

    let inferFromEmpty term = inferFrom [] term