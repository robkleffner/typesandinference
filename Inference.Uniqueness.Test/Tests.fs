namespace Inference.Uniqueness.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

open Inference.Uniqueness
open Inference.Uniqueness.Terms
open Inference.Uniqueness.Kinds
open Inference.Uniqueness.Types
open Inference.Uniqueness.Environment
open Inference.Uniqueness.Inference

[<TestClass>]
type UniquenessTests () = 

    let initialEnv = [
        termVarBind "pair"
            (schemeType
                [("t", KArrow (KAttr, KData));
                 ("u", KAttr);
                 ("s", KArrow (KAttr, KData));
                 ("v", KAttr);
                 ("w", KAttr)]
                (funType sharedAttr (typeApp (typeVar "t" (KArrow (KAttr, KData))) (typeVar "u" KAttr))
                    (funType (TAttr (Boolean.BVar "u"))
                             (typeApp (typeVar "s" (KArrow (KAttr, KData))) (typeVar "v" KAttr))
                             (attrType
                                (typeApp
                                    (typeApp
                                        (typeCon "Pair" (KArrow (KData, KArrow (KData, KArrow (KAttr, KData)))))
                                        (typeApp (typeVar "t" (KArrow (KAttr, KData))) (typeVar "u" KAttr)))
                                    (typeApp (typeVar "s" (KArrow (KAttr, KData))) (typeVar "v" KAttr)))
                                (Boolean.BOr (Boolean.BOr (Boolean.BVar "u", Boolean.BVar "v"), Boolean.BVar "w"))))));
        termVarBind "fst"
            (schemeType
                [("t0", KArrow (KAttr, KData));
                 ("u1", KAttr);
                 ("t2", KArrow (KAttr, KData));
                 ("u3", KAttr);
                 ("u5", KAttr)]
                (funType sharedAttr
                    (typeApp
                        (typeApp
                            (attrType
                                (typeCon "Pair" (KArrow (KAttr, KArrow (KData, KArrow (KData, KData)))))
                                (Boolean.BOr (Boolean.BOr (Boolean.BVar "u1", Boolean.BVar "u3"), Boolean.BVar "u5")))
                            (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr)))
                        (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr)))
                    (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))));
        termVarBind "snd"
            (schemeType
            [("t0", KArrow (KAttr, KData));
             ("u1", KAttr);
             ("t2", KArrow (KAttr, KData));
             ("u3", KAttr);
             ("u5", KAttr)]
            (funType sharedAttr
                (typeApp
                    (typeApp
                        (attrType
                            (typeCon "Pair" (KArrow (KAttr, KArrow (KData, KArrow (KData, KData)))))
                            (Boolean.BOr (Boolean.BOr (Boolean.BVar "u1", Boolean.BVar "u3"), Boolean.BVar "u5")))
                        (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr)))
                    (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr)))
                (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))))
    ]

    [<TestMethod>]
    member this.IdFunc () =
        Assert.AreEqual(
            funType (TAttr (Boolean.BFalse))
                    (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))
                    (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr)),
            inferFromEmpty (babs "a" false (bvar "a")))

    [<TestMethod>]
    member this.ConstFunc () =
        Assert.AreEqual(
            funType sharedAttr (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))
                (funType (TAttr (Boolean.BVar "u1"))
                         (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))
                         (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))),
            inferFromEmpty (babs "a" false (babs "b" false (bvar "a"))))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            funType sharedAttr
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))
                (funType sharedAttr
                    (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))
                    (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))),
            inferFromEmpty (babs "x" false (babs "y" false (bvar "y"))))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            funType (TAttr (Boolean.BFalse))
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr))
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (typeVar "u1" KAttr)),
            inferFromEmpty (babs "x" false (bapp (babs "y" false (bvar "y")) (bvar "x"))))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            funType (TAttr (Boolean.BFalse))
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (TAttr (Boolean.BVar "u6")))
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) (TAttr (Boolean.BVar "u6"))),
            inferFromEmpty (babs "x" false (bapp (babs "y" false (bvar "x")) (bvar "x"))))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            funType (TAttr (Boolean.BFalse))
                (typeApp (typeVar "t4" (KArrow (KAttr, KData))) (typeVar "u5" KAttr))
                (typeApp (typeVar "t4" (KArrow (KAttr, KData))) (typeVar "u5" KAttr)),
            inferFromEmpty (blet "m" true (babs "a" false (bvar "a")) (bapp (bvar "m") (bvar "m"))))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            funType (TAttr (Boolean.BFalse))
                (typeApp (typeVar "t9" (KArrow (KAttr, KData))) (typeVar "u10" KAttr))
                (typeApp (typeVar "t9" (KArrow (KAttr, KData))) (typeVar "u10" KAttr)),
            inferFromEmpty (blet "s" false (blet "m" true (babs "a" false (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            funType sharedAttr
                (funType (typeVar "u1" KAttr)
                         (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))
                         (typeApp (typeVar "t4" (KArrow (KAttr, KData))) (typeVar "u5" KAttr)))
                (funType (TAttr (Boolean.BVar "u1"))
                         (typeApp (typeVar "t2" (KArrow (KAttr, KData))) (typeVar "u3" KAttr))
                         (typeApp (typeVar "t4" (KArrow (KAttr, KData))) (typeVar "u5" KAttr))),
            inferFromEmpty (babs "x" false (babs "y" false (bapp (bvar "x") (bvar "y")))))

    [<TestMethod>]
    member this.DupFunc () = 
        Assert.AreEqual(
            funType sharedAttr
                (typeApp (typeVar "t0" (KArrow (KAttr, KData))) sharedAttr)
                (attrType
                    (typeApp
                        (typeApp
                            (typeCon "Pair" (KArrow (KData, KArrow (KData, KArrow (KAttr, KData)))))
                            (typeApp (typeVar "t0" (KArrow (KAttr, KData))) sharedAttr))
                        (typeApp (typeVar "t0" (KArrow (KAttr, KData))) sharedAttr))
                    (Boolean.BVar "w5")),
            inferFrom initialEnv (babs "a" true (bapp (bapp (bvar "pair") (bvar "a")) (bvar "a"))))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" true (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)
