﻿namespace Inference.Units

module Inference =

    open Inference.Common.Common;
    open Kinds;
    open Types;
    open Terms;
    open Environment;

    type InferenceState = { Fresh: int seq; Environment: List<EnvironmentEntry> }


    // utilities for working with inference states
    let extractConstraint context =
        List.tryFindIndex (isConstraint) context
        |> Option.map (fun constrInd -> (List.take constrInd context, List.item constrInd context, List.skip (constrInd + 1) context))

    let freshVar (prefix: string) =
        state
            {
            let! inf = get
            let i = Seq.head inf.Fresh
            do! put { Fresh = Seq.tail inf.Fresh; Environment = inf.Environment }
            return prefix.Substring(0, 1) + i.ToString()
            }

    let getEnvironment =
        state
            {
            let! inf = get
            return inf.Environment
            }

    let updateEnvironment f =
        state
            {
            let! inf = get
            do! put { Fresh = inf.Fresh; Environment = f inf.Environment }
            }

    let putEnvironment context = updateEnvironment (constant context)

    let pushEntry entry = updateEnvironment (fun con -> List.append con [entry])

    let addIntro name kind = pushEntry (typeVarIntro name kind)

    let addBinding name ty = pushEntry (termVarBind name ty)

    let popBinding name =
        let nameBinding item =
            match item with
            | ETermVarBind (n, _) when n = name -> true
            | _ -> false

        updateEnvironment (List.filter (nameBinding >> not))


    // constraint solving in context
    let rec makeHull ty =
        state
            {
            match ty with
            | TUnit u ->
                let! fresh = freshVar "h"
                return typeVar fresh KUnit, [(fresh, u)]
            | TApp (l, r) ->
                let! (lh, lds) = makeHull l
                let! (rh, rds) = makeHull r
                return typeApp lh rh, List.append lds rds
            | _ -> return ty, []
            }

    let decomposeIntoHullConstraint var kind rigid =
        state
            {
            let! (hull, constrs) = makeHull rigid
            let deps = List.map (fun (c, _) -> typeVarIntro c KUnit) constrs
            let unitConstraints = List.map (fun (c, u) -> binaryUnitConstr (simpleVarUnit c) u) constrs
            return flexRigidConstr var kind hull deps :: unitConstraints
            }

    let solveFlexFlex left right kind top =
        match top with
        | ETypeVarIntro (n, k) ->
            match (left = n, right = n) with
            | (true, true) -> [top]
            | (true, false) -> [typeVarDef n (typeVar right kind)]
            | (false, true) -> [typeVarDef n (typeVar left kind)]
            | (false, false) -> [flexFlexConstr left right kind; top]
        | ETypeVarDef (n, _) when left = n && right = n ->
            [top]
        | ETypeVarDef (n, def) ->
            [simpleBinaryConstr (typeSubst n def (typeVar left kind)) (typeSubst n def (typeVar right kind)); top]
        | _ ->
            [flexFlexConstr left right kind; top]

    let solveFlexRigid name kind ty deps top =
        match top with
        | ETypeVarDef (n, def) ->
            let constr =
                if (typeFree ty |> Set.add name).Contains n
                then simpleBinaryConstr (typeSubst n def (typeVar name kind)) (typeSubst n def ty)
                else flexRigidConstr name kind ty []
            List.append deps [constr; top]
        | ETypeVarIntro (n, k) ->
            match (name = n, (typeFree ty).Contains(n)) with
            | (true, true) -> failwith "Occurs check failed"
            | (true, false) -> List.append deps [typeVarDef name ty]
            | (false, true) -> [flexRigidConstr name kind ty (top :: deps)]
            | (false, false) -> [flexRigidConstr name kind ty deps; top]
        | _ -> [flexRigidConstr name kind ty deps; top]

    let solveTypeConstraint left right =
        state
            {
            match (left, right) with
            | (TCon (ln, lk), TCon (rn, rk)) when ln = rn && lk = rk ->
                return []
            | (TApp (ll, lr), TApp (rl, rr)) ->
                return [simpleBinaryConstr ll rl; simpleBinaryConstr lr rr]
            | (TVar (ln, lk), TVar (rn, rk)) when lk = rk ->
                return [flexFlexConstr ln rn lk]
            | (TUnit lu, TUnit ru) ->
                return [binaryUnitConstr lu ru]
            | (TVar (ln, lk), r) when lk = typeKind r ->
                return! decomposeIntoHullConstraint ln lk r
            | (l, TVar (rn, rk)) when rk = typeKind l ->
                return! decomposeIntoHullConstraint rn rk l
            | _ ->
                return failwith "Rigid-rigid mismatch or kinds do not unify"
            }

    let solveUnitConstraint (unit: Abelian.Equation<string, string>) dep top =
        state
            {
            if unit.IsIdentity()
            then return []
            else if unit.IsConstant()
            then return failwith "Unit mismatch"
            else
                match top with
                | ETypeVarDef (defName, defType) when unit.ExponentOf(defName) <> 0 ->
                    let subUnitType =
                        match defType with
                        | TUnit u -> u
                        | TVar (n, KUnit) -> simpleVarUnit n
                        | _ -> failwith "Invalid unit constraint solving state"
                    let intro = Option.map (fun name -> [typeVarIntro name KUnit]) dep |> Option.defaultValue []
                    return List.append intro [simpleUnitConstr (unit.Substitute defName subUnitType) dep; top]
                | ETypeVarIntro (name, kind) ->
                    if unit.ExponentOf name = 0
                    then return [simpleUnitConstr unit dep; top]
                    else if unit.DividesPowers (unit.ExponentOf name)
                    then
                        let pivoted = TUnit (unit.Pivot name)
                        match dep with
                        | Some depName -> return [typeVarIntro depName KUnit; typeVarDef name pivoted]
                        | None -> return [typeVarDef name pivoted]
                    else if unit.NotMax(name)
                    then
                        let! fresh = freshVar "b"
                        let pivoted = unit.Pivot(name).Add(simpleVarUnit fresh)
                        return [simpleUnitConstr (unit.Substitute name pivoted) dep; typeVarDef name (TUnit pivoted)]
                    else if unit.Variables.Count > 1
                    then return [simpleUnitConstr unit (Some name)]
                    else return failwith "Invalid unit constraint solving state"
                | _ -> return [simpleUnitConstr unit dep; top]
            }

    let solveOrMoveUp prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        append3 popped (fn top) suffix |> putEnvironment

    let solveOrMoveUpWithState prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        state
            {
            let! constrs = fn top
            do! append3 popped constrs suffix |> putEnvironment
            }

    let solveConstraint prefix suffix constr =
        match constr with
        | ETypeConstraint (left, right) ->
            state
                {
                let! constrs = solveTypeConstraint left right
                do! append3 prefix constrs suffix |> putEnvironment
                }
        | EFlexFlexConstraint (left, right, kind) ->
            solveOrMoveUp prefix suffix (solveFlexFlex left right kind)
        | EFlexRigidHullConstraint (var, kind, rigid, deps) ->
            solveOrMoveUp prefix suffix (solveFlexRigid var kind rigid deps)
        | EUnitConstraint (unit, dep) ->
            solveOrMoveUpWithState prefix suffix (solveUnitConstraint unit dep)
        | _ -> failwith "Tried to solve a non-constraint context entry"

    let rec solve () =
        state
            {
            let! env = getEnvironment
            match extractConstraint env with
            | Some (prefix, constr, suffix) ->
                do! solveConstraint prefix suffix constr
                do! solve ()
            | None -> do! putEnvironment env
            }


    // inference: from terms to typed terms
    let specialize poly =
        let rec specialLoop quant body =
            state
                {
                match quant with
                | q :: qs ->
                    let! fresh = freshVar (fst q)
                    do! addIntro fresh (snd q)
                    return! specialLoop qs (typeSubst (fst q) (typeVar fresh (snd q)) body)
                | [] -> return body
                }
        specialLoop poly.Quantified poly.Body

    let unify left right =
        state
            {
            do! pushEntry (simpleBinaryConstr left right)
            do! solve ()
            }

    let skimContext =
        state
            {
            let! context = getEnvironment
            let skimmed = List.rev context |> List.takeWhile (isLocalityMark >> not)
            do! putEnvironment (List.rev context |> List.skipWhile (isLocalityMark >> not) |> List.skip 1 |> List.rev)
            return skimmed
            }

    let rec infer term =
        state
            {
            match term with
            | BVar v ->
                let! context = getEnvironment
                match getTermVarType v context with
                | Some t -> return! specialize t
                | None -> return failwith "Could not find binding in context"
            | BAbs (p, b) ->
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! addBinding p (schemeType [] (typeVar fresh KData))
                let! rinf = infer b
                do! popBinding p
                return funType (typeVar fresh KData) rinf
            | BApp (l, r) ->
                let! linf = infer l
                let! rinf = infer r
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! unify linf (funType rinf (typeVar fresh KData))
                return typeVar fresh KData
            | BLet (l, e, b) ->
                let! geninf = inferGeneralized e
                do! addBinding l geninf
                let! bodyinf = infer b
                do! popBinding l
                return bodyinf
            | BFloat _ ->
                return typeFloat (typeUnit Map.empty Map.empty)
            }
    and inferGeneralized term =
        state
            {
            do! pushEntry ELocalityMark
            let! ungen = infer term
            let! generalized = skimContext
            return makeScheme generalized ungen
            }

    let inferFrom environment term =
        let (inf, final) = run { Fresh = Seq.initInfinite id; Environment = environment } (infer term)
        let normalized = normalizeEnv final.Environment
        applyEnvType normalized inf

    let inferFromEmpty term = inferFrom [] term