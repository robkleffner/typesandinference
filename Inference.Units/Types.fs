﻿namespace Inference.Units

module Types =
    
    open Inference.Common.Common
    open Kinds

    type Type =
        | TVar of string * Kind
        | TCon of string * Kind
        | TApp of Type * Type
        | TUnit of Abelian.Equation<string, string>
        override this.ToString () =
            match this with
            | TVar (n, k) -> n
            | TCon (n, k) -> n
            | TApp (l, r) -> $"({l} {r})"
            | TUnit u -> $"<{u}>"

    type TypeScheme = { Quantified: List<(string * Kind)>; Body: Type }


    // Functional constructors
    let typeVar n k = TVar (n, k)
    let typeCon n k = TCon (n, k)
    let typeApp l r = TApp (l, r)
    let typeUnit vars consts = TUnit (new Abelian.Equation<string, string>(vars, consts))
    let typeFloat arg = typeApp (typeCon "float" (KArrow (KUnit, KData))) arg
    let simpleVarUnit var = new Abelian.Equation<string, string>(Map.add var 1 Map.empty, Map.empty)
    let simpleConstUnit con = new Abelian.Equation<string, string>(Map.empty, Map.add con 1 Map.empty)

    let schemeType quantified body = { Quantified = quantified; Body = body }

    let funType inp outp = typeApp (typeApp (typeCon "->" (KArrow (KData, KArrow (KData, KData)))) inp) outp


    // Free variable computations
    let rec typeFree t =
        match t with
        | TVar (n, _) -> Set.singleton n
        | TCon _ -> Set.empty
        | TApp (l, r) -> Set.union (typeFree l) (typeFree r)
        | TUnit u -> u.Free()

    let schemeFree s = Set.difference (typeFree s.Body) (Set.ofList (List.map fst s.Quantified))


    // Substitution computations
    let rec typeSubst var sub target =
        match target with
        | TVar (n, _) -> if var = n then sub else target
        | TCon _ -> target
        | TApp (l, r) -> TApp (typeSubst var sub l, typeSubst var sub r)
        | TUnit u when u.Free().Contains(var) ->
            match sub with
            | TUnit subu -> TUnit (u.Substitute var subu)
            | TVar (s, k) when k = KUnit -> TUnit (u.Substitute var (simpleVarUnit s))
            | _ -> failwith "Cannot substitute a non-unit in a unit"
        | TUnit _ -> target

    let applySubstType subst target = Map.fold (fun ty var sub -> typeSubst var sub ty) target subst

    let composeSubst subl subr = Map.map (fun _ v -> applySubstType subl v) subr |> mapUnion fst subl


    // Kind computations
    let rec typeKind t =
        match t with
        | TVar (_, k) -> k
        | TCon (_, k) -> k
        | TApp (l, r) -> applyKind (typeKind l) (typeKind r)
        | TUnit _ -> KUnit
