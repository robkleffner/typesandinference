﻿namespace Inference.ScopedLabels

module Types =

    open Inference.Common.Common
    open Kinds

    type Type =
        | TVar of string * Kind
        | TCon of string * Kind
        | TApp of Type * Type
        | TField of string * Type * Type

    type TypeScheme = { Quantified: List<(string * Kind)>; Body: Type }
    
    
    // Functional constructors
    let typeVar n k = TVar (n, k)
    let typeCon n k = TCon (n, k)
    let typeApp l r = TApp (l, r)
    let typeField f v r = TField (f, v, r)
    
    let schemeType quantified body = { Quantified = quantified; Body = body }
    
    let funType inp outp = typeApp (typeApp (typeCon "->" (KArrow (KData, (KArrow (KData, KData))))) inp) outp

    let emptyRecordType = typeApp (typeCon "{}" (KArrow (KRow, KData))) (typeCon "empty" KRow)

    let recordType row = typeApp (typeCon "{}" (KArrow (KRow, KData))) row

    let variantType row = typeApp (typeCon "<>" (KArrow (KRow, KData))) row
    
    
    // Free variable computations
    let rec typeFree t =
        match t with
        | TVar (n, _) -> Set.singleton n
        | TCon _ -> Set.empty
        | TApp (l, r) -> Set.union (typeFree l) (typeFree r)
        | TField (_, v, r) -> Set.union (typeFree v) (typeFree r)
    
    let schemeFree s = Set.difference (typeFree s.Body) (Set.ofList (List.map fst s.Quantified))
    
    
    // Kind computations
    let rec typeKind (t : Type) =
        match t with
        | TVar (_, k) -> k
        | TCon (_, k) -> k
        | TApp (l, r) -> applyKind (typeKind l) (typeKind r)
        | TField (_, v, r) ->
            if typeKind v = KData && typeKind r = KRow
            then KRow
            else failwith "Field requires data kind argument and must extended a row kind"


    // Kind-preserving substitution computations
    let rec typeSubst var sub target =
        match target with
        | TVar (n, k) -> if var = n && typeKind sub = k then sub else target
        | TCon _ -> target
        | TApp (l, r) -> TApp (typeSubst var sub l, typeSubst var sub r)
        | TField (f, v, r) -> TField (f, typeSubst var sub v, typeSubst var sub r)
    
    let applySubstType subst target = Map.fold (fun ty var sub -> typeSubst var sub ty) target subst
    
    let composeSubst subl subr = Map.map (fun _ v -> applySubstType subl v) subr |> mapUnion fst subl