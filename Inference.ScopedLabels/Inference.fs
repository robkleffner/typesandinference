﻿namespace Inference.ScopedLabels

module Inference =

    open Inference.Common.Common;
    open Kinds;
    open Types;
    open Terms;
    open Environment;

    type InferenceState = { Fresh: int seq; Environment: List<EnvironmentEntry> }


    // utilities for working with inference states
    let extractConstraint context =
        List.tryFindIndex (isConstraint) context
        |> Option.map (fun constrInd -> (List.take constrInd context, List.item constrInd context, List.skip (constrInd + 1) context))

    let freshVar (prefix: string) =
        state
            {
            let! inf = get
            let i = Seq.head inf.Fresh
            do! put { Fresh = Seq.tail inf.Fresh; Environment = inf.Environment }
            return prefix.Substring(0, 1) + i.ToString()
            }

    let getEnvironment =
        state
            {
            let! inf = get
            return inf.Environment
            }

    let updateEnvironment f =
        state
            {
            let! inf = get
            do! put { Fresh = inf.Fresh; Environment = f inf.Environment }
            }

    let putEnvironment context = updateEnvironment (constant context)

    let pushEntry entry = updateEnvironment (fun con -> List.append con [entry])

    let addIntro name kind = pushEntry (typeVarIntro name kind)

    let addBinding name ty = pushEntry (termVarBind name ty)

    let popBinding name =
        let nameBinding item =
            match item with
            | ETermVarBind (n, _) when n = name -> true
            | _ -> false

        updateEnvironment (List.filter (nameBinding >> not))


    // constraint solving in context
    let solveFlexFlex left right kind top =
        match top with
        | ETypeVarIntro (n, k) ->
            match (left = n, right = n) with
            | (true, true) -> [top]
            | (true, false) -> [typeVarDef n (typeVar right kind)]
            | (false, true) -> [typeVarDef n (typeVar left kind)]
            | (false, false) -> [flexFlexConstr left right kind; top]
        | ETypeVarDef (n, _) when left = n && right = n ->
            [top]
        | ETypeVarDef (n, def) ->
            [simpleBinaryConstr (typeSubst n def (typeVar left kind)) (typeSubst n def (typeVar right kind)); top]
        | _ ->
            [flexFlexConstr left right kind; top]

    let solveFlexRigid name kind ty deps top =
        match top with
        | ETypeVarDef (n, def) ->
            let constr =
                if (typeFree ty |> Set.add name).Contains n
                then simpleBinaryConstr (typeSubst n def (typeVar name kind)) (typeSubst n def ty)
                else flexRigidConstr name kind ty []
            List.append deps [constr; top]
        | ETypeVarIntro (n, k) ->
            match (name = n, (typeFree ty).Contains(n)) with
            | (true, true) -> failwith "Occurs check failed"
            | (true, false) -> List.append deps [typeVarDef name ty]
            | (false, true) -> [flexRigidConstr name kind ty (top :: deps)]
            | (false, false) -> [flexRigidConstr name kind ty deps; top]
        | _ -> [flexRigidConstr name kind ty deps; top]

    let solveTypeConstraint left right =
        state
            {
            match (left, right) with
            | (TCon (ln, lk), TCon (rn, rk)) when ln = rn && lk = rk ->
                return []
            | (TApp (ll, lr), TApp (rl, rr)) ->
                return [simpleBinaryConstr ll rl; simpleBinaryConstr lr rr]
            | (TVar (ln, lk), TVar (rn, rk)) when lk = rk ->
                return [flexFlexConstr ln rn lk]
            | (TField _, TField _) ->
                return [fieldsToRowConstr left right]
            | (TVar (ln, lk), r) when lk = typeKind r ->
                return [flexRigidConstr ln lk r []]
            | (l, TVar (rn, rk)) when rk = typeKind l ->
                return [flexRigidConstr rn rk l []]
            | _ ->
                return failwith "Rigid-rigid mismatch or kinds do not unify"
            }

    let overlappingFields left right = Set.intersect (Set.ofList (List.map fst left)) (Set.ofList (List.map fst right))

    let decomposeMatchingFields overlapped lefts rights leftRow rightRow =
        let field = Set.minElement overlapped
        let fieldType fs = List.find (fun p -> field = fst p) fs |> snd
        let removeField fs = List.filter (fun p -> field <> fst p) fs
        [simpleBinaryConstr (fieldType lefts) (fieldType rights); rowConstr (removeField lefts) (removeField rights) leftRow rightRow]

    let solveRowConstraint lefts rights leftRow rightRow =
        state
            {
            match lefts, rights with
            | [], [] -> return [simpleBinaryConstr leftRow rightRow]
            | ls, [] ->
                match rightRow with
                | TVar (n, KRow) -> return [flexRigidConstr n KRow (rowConstrSideToField ls leftRow) []]
                | _ -> return failwith "Rigid-row mismatch or kinds do not unify"
            | [], rs ->
                match leftRow with
                | TVar (n, KRow) -> return [flexRigidConstr n KRow (rowConstrSideToField rs rightRow) []]
                | _ -> return failwith "Rigid-row mismatch or kinds do not unify"
            | ls, rs ->
                let overlapped = overlappingFields ls rs
                if not (Set.isEmpty overlapped)
                then return decomposeMatchingFields overlapped ls rs leftRow rightRow
                else
                    match leftRow, rightRow with
                    | TVar (ln, _), TVar (rn, _) when ln = rn -> return failwith "Conflicting rows do not unify"
                    | TVar (ln, _), TVar (rn, _) ->
                        let! fresh = freshVar "r"
                        do! addIntro fresh KRow
                        return [flexRigidConstr ln KRow (rowConstrSideToField rs (typeVar fresh KRow)) [];
                                flexRigidConstr rn KRow (rowConstrSideToField ls (typeVar fresh KRow)) []]
                    | _ -> return failwith "Conflicting rows do not unify"
            }
            
    let solveInPlace prefix suffix fn =
        state
            {
            let! constrs = fn
            do! append3 prefix constrs suffix |> putEnvironment
            }

    let solveOrMoveUp prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        append3 popped (fn top) suffix |> putEnvironment

    let solveOrMoveUpWithState prefix suffix fn =
        let top = List.last prefix
        let popped = List.take (prefix.Length - 1) prefix
        state
            {
            let! constrs = fn top
            do! append3 popped constrs suffix |> putEnvironment
            }

    let solveConstraint prefix suffix constr =
        match constr with
        | ETypeConstraint (left, right) ->
            solveInPlace prefix suffix (solveTypeConstraint left right)
        | EFlexFlexConstraint (left, right, kind) ->
            solveOrMoveUp prefix suffix (solveFlexFlex left right kind)
        | EFlexRigidConstraint (var, kind, rigid, deps) ->
            solveOrMoveUp prefix suffix (solveFlexRigid var kind rigid deps)
        | ERowConstraint (leftFields, rightFields, leftRow, rightRow) ->
            solveInPlace prefix suffix (solveRowConstraint leftFields rightFields leftRow rightRow)
        | _ -> failwith "Tried to solve a non-constraint context entry"

    let rec solve () =
        state
            {
            let! env = getEnvironment
            match extractConstraint env with
            | Some (prefix, constr, suffix) ->
                do! solveConstraint prefix suffix constr
                do! solve ()
            | None -> do! putEnvironment env
            }


    // inference: from terms to typed terms
    let specialize poly =
        let rec specialLoop quant body =
            state
                {
                match quant with
                | q :: qs ->
                    let! fresh = freshVar (fst q)
                    do! addIntro fresh (snd q)
                    return! specialLoop qs (typeSubst (fst q) (typeVar fresh (snd q)) body)
                | [] -> return body
                }
        specialLoop poly.Quantified poly.Body

    let unify left right =
        state
            {
            do! pushEntry (simpleBinaryConstr left right)
            do! solve ()
            }

    let skimContext =
        state
            {
            let! context = getEnvironment
            let skimmed = List.rev context |> List.takeWhile (isLocalityMark >> not)
            do! putEnvironment (List.rev context |> List.skipWhile (isLocalityMark >> not) |> List.skip 1 |> List.rev)
            return skimmed
            }

    let rec infer term =
        state
            {
            match term with
            | BVar v ->
                let! context = getEnvironment
                match getTermVarType v context with
                | Some t -> return! specialize t
                | None -> return failwith "Could not find binding in context"
            | BAbs (p, b) ->
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! addBinding p (schemeType [] (typeVar fresh KData))
                let! rinf = infer b
                do! popBinding p
                return funType (typeVar fresh KData) rinf
            | BApp (l, r) ->
                let! linf = infer l
                let! rinf = infer r
                let! fresh = freshVar "t"
                do! addIntro fresh KData
                do! unify linf (funType rinf (typeVar fresh KData))
                return typeVar fresh KData
            | BLet (l, e, b) ->
                let! geninf = inferGeneralized e
                do! addBinding l geninf
                let! bodyinf = infer b
                do! popBinding l
                return bodyinf
            | BRecord ->
                return recordType (typeCon "empty" KRow)
            | BSelect field ->
                let! freshArg = freshVar "a"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRow KRow
                return funType (recordType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow))) (typeVar freshArg KData)
            | BRestrict field ->
                let! freshArg = freshVar "a"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRow KRow
                return funType (recordType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow))) (recordType (typeVar freshRow KRow))
            | BExtend field ->
                let! freshArg = freshVar "a"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRow KRow
                return funType (typeVar freshArg KData)
                        (funType (recordType (typeVar freshRow KRow))
                            (recordType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow))))
            | BInject field ->
                let! freshArg = freshVar "a"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRow KRow
                return funType (typeVar freshArg KData) (variantType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow)))
            | BEmbed field ->
                let! freshArg = freshVar "a"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRow KRow
                return funType (typeVar freshRow KRow) (variantType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow)))
            | BCase field ->
                let! freshArg = freshVar "a"
                let! freshRes = freshVar "t"
                let! freshRow = freshVar "r"
                do! addIntro freshArg KData
                do! addIntro freshRes KData
                do! addIntro freshRow KRow
                return funType (variantType (typeField field (typeVar freshArg KData) (typeVar freshRow KRow)))
                        (funType (funType (typeVar freshArg KData) (typeVar freshRes KData))
                            (funType (funType (variantType (typeVar freshRow KRow)) (typeVar freshRes KData))
                                (typeVar freshRes KData)))
            }
    and inferGeneralized term =
        state
            {
            do! pushEntry ELocalityMark
            let! ungen = infer term
            let! generalized = skimContext
            return makeScheme generalized ungen
            }

    let inferFrom environment term =
        let (inf, final) = run { Fresh = Seq.initInfinite id; Environment = environment } (infer term)
        let normalized = normalizeEnv final.Environment
        applyEnvType normalized inf

    let inferFromEmpty term = inferFrom [] term