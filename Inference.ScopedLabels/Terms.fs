﻿namespace Inference.ScopedLabels

module Terms =

    type Term =
        | BVar of string
        | BAbs of string * Term
        | BApp of Term * Term
        | BLet of string * Term * Term
        | BRecord
        | BSelect of string
        | BRestrict of string
        | BExtend of string
        | BInject of string
        | BEmbed of string
        | BCase of string

    let bvar name = BVar name

    let babs param tm = BAbs (param, tm)

    let bapp left right = BApp (left, right)

    let blet name named exp = BLet (name, named, exp)