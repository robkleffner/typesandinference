﻿namespace Inference.ScopedLabels

module Kinds =

    type Kind =
        | KData
        | KRow
        | KArrow of Kind * Kind

    let applyKind kfun karg =
        match kfun with
        | KArrow (input, output) ->
            if input = karg
            then output
            else failwith "Cannot apply kind to input argument: input kind must match argument kind"
        | _ -> failwith "Cannot apply a non-arrow kind"