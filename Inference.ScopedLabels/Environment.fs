﻿namespace Inference.ScopedLabels

module Environment =

    open Kinds
    open Types

    type EnvironmentEntry =
        | ETypeVarIntro of string * Kind
        | ETypeVarDef of string * Type
        | ETermVarBind of string * TypeScheme
        | ELocalityMark
        | ETypeConstraint of left: Type * right: Type
        | EFlexFlexConstraint of left: string * right: string * kind: Kind
        | EFlexRigidConstraint of var: string * kind: Kind * rigid: Type * dependencies: List<EnvironmentEntry>
        | ERowConstraint of leftFields: List<(string*Type)> * rightFields: List<(string*Type)> * leftRow: Type * rightRow: Type


    // utilities for working with environment entries
    let typeVarIntro name kind = ETypeVarIntro (name, kind)

    let typeVarDef name def = ETypeVarDef (name, def)

    let termVarBind name ty = ETermVarBind (name, ty)

    let simpleBinaryConstr left right = ETypeConstraint (left, right)

    let flexFlexConstr left right kind = EFlexFlexConstraint (left, right, kind)

    let flexRigidConstr var kind rigid deps = EFlexRigidConstraint (var, kind, rigid, deps)

    let rowConstr lefts rights lr rr = ERowConstraint (lefts, rights, lr, rr)

    let fieldsToRowConstr leftRow rightRow =
        let rec typeRowToConstraintRow acc tyrow =
            match tyrow with
            | TField (f, a, r) -> typeRowToConstraintRow ((f,a) :: acc) r
            | _ -> (acc, tyrow)
        let (leftFields, leftRow) = typeRowToConstraintRow [] leftRow
        let (rightFields, rightRow) = typeRowToConstraintRow [] rightRow
        ERowConstraint (leftFields, rightFields, leftRow, rightRow)

    let rec rowConstrSideToField fields rowEnd =
        match fields with
        | (f, a) :: fs -> typeField f a (rowConstrSideToField fs rowEnd)
        | [] -> rowEnd

    let isConstraint entry =
        match entry with
        | ETypeConstraint _ -> true
        | EFlexFlexConstraint _ -> true
        | EFlexRigidConstraint _ -> true
        | ERowConstraint _ -> true
        | _ -> false

    let isLocalityMark entry =
        match entry with
        | ELocalityMark -> true
        | _ -> false

    let typeVarName entry =
        match entry with
        | ETypeVarIntro (n, _) -> n
        | ETypeVarDef (n, _) -> n
        | _ -> failwith "Expected a type variable entry"

    let constraintFields fields = List.map fst fields |> Set.ofList

    let sharedConstrainFields lefts rights = Set.intersect (constraintFields lefts) (constraintFields rights)


    // substitution computations
    let applyEntryType t entry =
        match entry with
        | ETypeVarDef (n, def) -> typeSubst n def t
        | _ -> t

    let applyEnvType env t = List.fold applyEntryType t env


    // accessors over contexts
    let rec getTermVarType var env =
        match env with
        | ETermVarBind (n, t) :: _ when n = var -> Option.Some t
        | _ :: cs -> getTermVarType var cs
        | [] -> Option.None

    
    // other utilities
    let makeScheme generalized ty =
        let mutable body = ty
        let mutable quantified = List.empty
        for g in generalized do
            match g with
            | ETypeVarIntro (n, k) -> quantified <- (n, k) :: quantified
            | ETypeVarDef (n, d) -> body <- typeSubst n d body
            | _ -> failwith "Unexpected entry in skimmed context"
        schemeType quantified body

    let normalizeEntry env entry =
        match entry with
        | ETypeVarDef (n, t) -> ETypeVarDef (n, applyEnvType env t)
        | _ -> entry

    let normalizeEnv = List.fold (fun prev next -> normalizeEntry prev next :: prev) [] >> List.rev