namespace Inference.ScopedLabels.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

open Inference.ScopedLabels.Kinds
open Inference.ScopedLabels.Terms
open Inference.ScopedLabels.Types
open Inference.ScopedLabels.Inference

[<TestClass>]
type BasicTests () =

    [<TestMethod>]
    member this.IdentityFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bvar "x")),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.DropFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "y"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t1" KData)))

    [<TestMethod>]
    member this.ConstFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bvar "x"))),
            funType (typeVar "t0" KData) (funType (typeVar "t1" KData) (typeVar "t0" KData)))

    [<TestMethod>]
    member this.NestedIdFn () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "y")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.UnusedInp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (bapp (babs "y" (bvar "x")) (bvar "x"))),
            funType (typeVar "t0" KData) (typeVar "t0" KData))

    [<TestMethod>]
    member this.IdSelfApp () =
        Assert.AreEqual(
            inferFromEmpty (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))),
            funType (typeVar "t2" KData) (typeVar "t2" KData))

    [<TestMethod>]
    member this.IdSelfAppNested () =
        Assert.AreEqual(
            inferFromEmpty (blet "s" (blet "m" (babs "a" (bvar "a")) (bapp (bvar "m") (bvar "m"))) (bvar "s")),
            funType (typeVar "t4" KData) (typeVar "t4" KData))

    [<TestMethod>]
    member this.FnApp () =
        Assert.AreEqual(
            inferFromEmpty (babs "x" (babs "y" (bapp (bvar "x") (bvar "y")))),
            funType (funType (typeVar "t1" KData) (typeVar "t2" KData)) (funType (typeVar "t1" KData) (typeVar "t2" KData)))

    [<TestMethod>]
    member this.SelfAppFails () =
        Assert.ThrowsException(fun () -> inferFromEmpty (babs "x" (bapp (bvar "x") (bvar "x"))) |> ignore) |> ignore
        Assert.IsTrue(true)

[<TestClass>]
type RowTests () =

    [<TestMethod>]
    member this.UnambiguousExtension () =
        Assert.AreEqual(
            inferFromEmpty (babs "r"
                (bapp
                    (bapp
                        (bapp (BCase "true") (bapp (BInject "true") BRecord))
                        (babs "a" (bapp (bapp (BExtend "x") BRecord) (bvar "r"))))
                    (babs "b" (bapp (bapp (BExtend "x") BRecord) BRecord)))),
            funType (emptyRecordType) (recordType (typeField "x" emptyRecordType (typeCon "empty" KRow))))

    [<TestMethod>]
    member this.GuardAgainstDivergedUnification () =
        Assert.ThrowsException(fun () ->
            inferFromEmpty (babs "r"
                (bapp
                    (bapp
                        (bapp (BCase "true") (bapp (BInject "true") BRecord))
                        (babs "a" (bapp (bapp (BExtend "x") BRecord) (bvar "r"))))
                    (babs "b" (bapp (bapp (BExtend "y") BRecord) (bvar "r"))))) |> ignore) |> ignore
        Assert.IsTrue(true)

    [<TestMethod>]
    member this.DoubleSelection () =
        Assert.AreEqual(
            inferFromEmpty (babs "r"
                (bapp (bapp (BSelect "one") (bvar "r")) (bapp (BSelect "two") (bvar "r")))),
            funType
                (recordType
                    (typeField "one" (funType (typeVar "a4" KData) (typeVar "t8" KData))
                        (typeField "two" (typeVar "a4" KData)
                            (typeVar "r7" KRow))))
                (typeVar "t8" KData))